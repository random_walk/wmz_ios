//
//  DDProgressBarView.h
//  dd
//
//  Created by darkdong on 14-3-5.
//  Copyright (c) 2014年 paixiu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DDProgressView.h"

@interface DDProgressBarView : DDProgressView

@property (nonatomic) CGFloat cornerRadius;
@property (nonatomic, retain) UIColor *progressColor;

@end
