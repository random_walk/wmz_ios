//
//  DDSharedPlayer.h
//  dd
//
//  Created by darkdong on 13-6-4.
//  Copyright (c) 2013年 darkdong. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol DDSharedPlayerDelegate <NSObject>

@optional
- (void)playDidEnd;

@end

@class AVPlayer;

@interface DDSharedPlayer : NSObject

@property (retain) AVPlayer *avPlayer;
@property BOOL loopingPlay;
@property (retain) NSURL *url;

+ (DDSharedPlayer *)player;
- (void)play;
- (void)pause;
- (void)clear;
- (BOOL)isPlaying;
@end
