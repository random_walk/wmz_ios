//
//  DDUtility.h
//  dd
//
//  Created by darkdong on 13-3-19.
//  Copyright (c) 2013年 darkdong. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
#import "DDType.h"
#import "NSArray+Additions.h"

extern NSString * const DDArrayMergingModeKeyMaxID;
extern NSString * const DDArrayMergingModeKeyMinID;

//parse
BOOL DUObjectIsString(id obj);
BOOL DUObjectIsStringOrNumber(id obj);
BOOL DUObjectIsArray(id obj);
id DUJSONObjectWithData(NSData *data);
BOOL DUResponseObjectIsSuccess(id obj);

//os info
CGFloat DUGetStatusBarHeight(void);
BOOL DUDeviceIsLongScreen(void);
NSArray * DUGetAllFontNames(void);

//app info
NSString * DUGetAppVersion(void);

//dispatch queue
dispatch_queue_t DUDispatchGetMainQueue(void);
dispatch_queue_t DUDispatchGetGlobalQueue(void);

//string
NSString * DUGetUUID(void);
NSString * DUGetStringFromFourCharCode(UInt32 fourCharCode);
BOOL DUVersionGreaterOrEqualTo(NSString *version1, NSString *version2);
BOOL DUVersionLessThan(NSString *version1, NSString *version2);
BOOL DUSystemVersionGreatOrEqualTo(NSString *version);

//color
UIColor* DUGetTransparentColorForClick(void);

//file system
NSArray * DUFSGetOrderedFiles(NSString *directory, BOOL excludeHiddenFiles);
NSString * DUFSGetNextOrderPlistFilePath(NSString *directory);
void DUFSDeleteAllFilesInDirectory(NSString *directory);
unsigned long long DUFSGetDirectorySize(NSString *directory);
NSString * DUFSGetSizeString(unsigned long long size);

//merging array
NSDictionary * DUMakeArrayMergingModeInfo(NSArray *models, DDArrayMergingMode mode, SEL comparator, NSDictionary *customKeyInfo);

//AVFoundation
AVCaptureConnection * DUCaptureSessionGetConnection(AVCaptureSession *session, NSString *mediaType);
AVCaptureConnection * DUCaptureOutputGetConnection(AVCaptureOutput *output, NSString *mediaType);
AVCaptureDeviceInput * DUCaptureSessionGetDeviceInput(AVCaptureSession *session, NSString *mediaType);
AVCaptureOutput * DUCaptureSessionGetOutputByClass(AVCaptureSession *session, Class outputClass);
AVCaptureOutput * DUCaptureSessionGetOutput(AVCaptureSession *session, NSString *mediaType);
AVCaptureDevice * DUCaptureDeviceGetCamera(AVCaptureDevicePosition position);
AVCaptureDevicePosition DUCaptureDeviceGetCameraPosition(AVCaptureSession *session);
NSUInteger DUCaptureDeviceGetCameraCount(void);
void DUPlayWithVolume(AVPlayer *player, float volume);
void DUPlayInSilentMode(void);
UIImage * DUSampleBufferGetImage(CMSampleBufferRef sampleBuffer, AVCaptureDevicePosition devicePosition);
CGPoint DUCameraConvertPointOfInterestToView(CGPoint pointOfInterest, AVCaptureVideoPreviewLayer *videoPreviewLayer, AVCaptureSession *session, AVCaptureOutput *output);
CGPoint DUCameraConvertPointOfViewToInterest(CGPoint pointOfView, AVCaptureVideoPreviewLayer *videoPreviewLayer, AVCaptureSession *session, AVCaptureOutput *output);
void DUCameraSetFocus(AVCaptureDevice *device, CGPoint pointOfInterest, AVCaptureFocusMode focusMode);
void DUCameraSetExposure(AVCaptureDevice *device, CGPoint pointOfInterest, AVCaptureExposureMode exposureMode);
void DUCameraSetFlashMode(AVCaptureDevice *device, AVCaptureFlashMode flashMode);
void DUCameraSetTorchMode(AVCaptureDevice *device, AVCaptureTorchMode torchMode);
void DUCameraLockAEAF(AVCaptureDevice *device);

//photo
void DUPhotoAddGPSInfo(CLLocation *location, NSMutableDictionary *metaData);
void DUPhotoAddImageOrientation(UIImageOrientation imageOrientation, NSMutableDictionary *metaData);

//user defaults
NSUInteger DUUserDefaultsGetAllBitMask(void);
void DUUserDefaultsSetAllBitMask(NSUInteger allBitMask);
BOOL DUUserDefaultsHasBitMask(NSUInteger bitMask);
void DUUserDefaultsSetBitMask(NSUInteger bitMask, BOOL flag);

//view
void DUShowPopupView(UIView *popupView, float delta, float duration, DDBlockVoid completion);
void DUDismissPopupView(UIView *popupView, float delta, float duration, DDBlockVoid completion);
BOOL DUScrollViewIsCloseToBottom(UIScrollView *scrollView);
UIButton* DUMakeStretchableImageButton(NSString *imageName, NSString *highlightedImageName);
UIWindow* DUGetMainWindow(void);
void DUShowSharedAlertView(NSString *title, NSString *message, NSString *okLabel);

//controller
id DUGetMainController(void);

@interface DDUtility : NSObject

+ (BOOL)monitorContainsObject:(NSUInteger)objectHashCode;
+ (void)monitorAddObject:(NSUInteger)objectHashCode;
+ (void)monitorRemoveObject:(NSUInteger)objectHashCode;

@end
