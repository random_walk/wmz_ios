//
//  DDProgressPieView.h
//  dd
//
//  Created by darkdong on 14-3-5.
//  Copyright (c) 2014年 paixiu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DDProgressView.h"

@interface DDProgressPieView : DDProgressView

@property (nonatomic) UIColor *pieColor;
@property (nonatomic) UIColor *circleColor;

@end
