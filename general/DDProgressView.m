//
//  DDProgressView.m
//  dd
//
//  Created by darkdong on 13-11-28.
//  Copyright (c) 2013年 PixShow. All rights reserved.
//

#import "DDProgressView.h"
#import "DDPreprocessMacro.h"
#import "DDGlobal.h"
#import "DDTimerTarget.h"
#import "UIView+Additions.h"

static const NSTimeInterval kUpdateTimeInterval = 0.04;//每1/25秒更新一次产生动画效果
static const NSTimeInterval kDefaultAnimationDuration = 6 * kUpdateTimeInterval;//用6个更新周期完成进度动画
static const NSTimeInterval kLastAnimationDuration = 3 * kUpdateTimeInterval;//用3个更新周期完成最后一步的进度动画

@interface DDProgressView ()

@property (nonatomic) NSTimer *timer;
@property (nonatomic) CGFloat animationTargetProgress;
@property (nonatomic) CGFloat animationCurrentProgress;
@property (nonatomic) CGFloat stepProgress;

@end

@implementation DDProgressView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        DDLog(@"DDProgressView init %p", self);
        // Initialization code
//        _timer = [NSTimer timerWithTimeInterval:kUpdateTimeInterval target:self selector:@selector(moveProgress:) userInfo:nil repeats:YES];
//        [[NSRunLoop mainRunLoop] addTimer:_timer forMode:NSRunLoopCommonModes];
        _timer = [DDTimerTarget scheduledTimerWithTimeInterval:kUpdateTimeInterval target:self selector:@selector(moveProgress:) userInfo:nil repeats:YES];
    }
    return self;
}

- (void)dealloc {
    DDLog(@"DDProgressView dealloc %p", self);
    DD_INVALIDATE_TIMER(_timer);
}

- (CGFloat)progress {
    return self.animationTargetProgress;
}

- (void)setProgress:(CGFloat)progress
{
    [self setProgress:progress animated:NO];
}

#pragma mark - private

- (void)moveProgress:(id)timer
{
    if(self.animationCurrentProgress < self.animationTargetProgress) {
        self.animationCurrentProgress += self.stepProgress;
        if (self.animationCurrentProgress > self.animationTargetProgress) {
            self.animationCurrentProgress = self.animationTargetProgress;
        }
        [self setNeedsDisplay];
    }
}

#pragma mark - public

- (void)invalidateTimer {
    DD_INVALIDATE_TIMER(_timer);
}

- (void)setProgress:(CGFloat)progress animated:(BOOL)animated
{
    if (animated) {
        NSTimeInterval animationDuration = progress >= 1? kLastAnimationDuration: kDefaultAnimationDuration;
        [self setProgressAnimately:progress animationDuration:animationDuration];
    }else {
        self.animationTargetProgress = progress;
        self.animationCurrentProgress = progress;
        [self setNeedsDisplay];
    }
}

- (void)setProgressAnimately:(CGFloat)progress animationDuration:(NSTimeInterval)animationDuration {
    self.animationTargetProgress = progress;
    self.stepProgress = (self.animationTargetProgress - self.animationCurrentProgress) * kUpdateTimeInterval / animationDuration;
    [_timer fire];
}

@end
