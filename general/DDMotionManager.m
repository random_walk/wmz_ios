//
//  DDMotionManager.m
//  dd
//
//  Created by darkdong on 14-2-26.
//  Copyright (c) 2014年 paixiu. All rights reserved.
//

#import <CoreMotion/CoreMotion.h>
#import "DDMotionManager.h"
#import "DDPreprocessMacro.h"

NSString * const DDNotificationNameDeviceOrientationDidChange = @"DDNotificationNameDeviceOrientationDidChange";

@interface DDMotionManager ()

@property CMMotionManager *motionManager;

@end

@implementation DDMotionManager

+ (id)sharedMotionManager {
    static DDMotionManager *sharedMotionManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMotionManager = [[self alloc] init];
    });
    return sharedMotionManager;
}

- (id)init {
    if (self = [super init]) {
        self.deviceOrientation = UIDeviceOrientationPortrait;
        self.affineTransform = CGAffineTransformIdentity;
        self.motionManager = [[CMMotionManager alloc] init];
    }
    return self;
}

- (void)dealloc {
    // Should never be called, but just here for clarity really.
}

- (void)startAccelerometerUpdates:(NSTimeInterval)updateInterval {
    
    self.motionManager.accelerometerUpdateInterval = updateInterval;
    
    [self.motionManager startAccelerometerUpdatesToQueue:[NSOperationQueue currentQueue] withHandler:^(CMAccelerometerData *accelerometerData, NSError *error) {
        
        CMAcceleration acceleration = accelerometerData.acceleration;
        
        double absX = ABS(acceleration.x);
        double absY = ABS(acceleration.y);
        double absZ = ABS(acceleration.z);
        
//        DDLog(@"x %f y %f z %f", acceleration.x, acceleration.y, acceleration.z);
        
        UIAccelerationValue maxValue = MAX(MAX(absX, absY), absZ);
        UIDeviceOrientation deviceOrientation = self.deviceOrientation;
        
        if (maxValue == absY) {
            //portrait
            if (acceleration.y > 0) {
//                DDLog(@"UIDeviceOrientationPortraitUpsideDown");
                if (UIDeviceOrientationPortraitUpsideDown != self.deviceOrientation) {
                    deviceOrientation = UIDeviceOrientationPortraitUpsideDown;
                }
            }else {
//                DDLog(@"UIDeviceOrientationPortrait");
                if (UIDeviceOrientationPortrait != self.deviceOrientation) {
                    deviceOrientation = UIDeviceOrientationPortrait;
                }
            }
        }else if (maxValue == absX) {
            //landscape
            if (acceleration.x > 0) {
//                DDLog(@"UIDeviceOrientationLandscapeRight");
                if (UIDeviceOrientationLandscapeRight != self.deviceOrientation) {
                    deviceOrientation = UIDeviceOrientationLandscapeRight;
                }
            }else {
//                DDLog(@"UIDeviceOrientationLandscapeLeft");
                if (UIDeviceOrientationLandscapeLeft != self.deviceOrientation) {
                    deviceOrientation = UIDeviceOrientationLandscapeLeft;
                }
            }
        }else {
            //remain
//            DDLog(@"remain");
        }
        
        if (deviceOrientation != self.deviceOrientation) {
//            DDLog(@"device orientation did change from %d to %d", self.deviceOrientation, deviceOrientation);
            self.deviceOrientation = deviceOrientation;
            
            CGFloat radian = 0;
            if (UIDeviceOrientationLandscapeLeft == deviceOrientation) {
                radian = M_PI_2;
            }else if (UIDeviceOrientationLandscapeRight == deviceOrientation) {
                radian = -M_PI_2;
            }else if (UIDeviceOrientationPortraitUpsideDown == deviceOrientation) {
                radian = M_PI;
            }
            if (0 == radian) {
                self.affineTransform = CGAffineTransformIdentity;
            }else {
                self.affineTransform = CGAffineTransformMakeRotation(radian);
            }
            
            [[NSNotificationCenter defaultCenter] postNotificationName:DDNotificationNameDeviceOrientationDidChange object:nil userInfo:nil];
        }
    }];
}

- (void)stopAccelerometerUpdates {
    [self.motionManager stopAccelerometerUpdates];
}

@end
