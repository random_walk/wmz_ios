//
//  DDProgressView_Redeclare.h
//  dd
//
//  Created by darkdong on 14-3-5.
//  Copyright (c) 2014年 paixiu. All rights reserved.
//

#import "DDProgressView.h"

@interface DDProgressView ()

@property (nonatomic, readonly) CGFloat animationCurrentProgress;

@end
