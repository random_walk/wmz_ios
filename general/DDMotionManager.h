//
//  DDMotionManager.h
//  dd
//
//  Created by darkdong on 14-2-26.
//  Copyright (c) 2014年 paixiu. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString * const DDNotificationNameDeviceOrientationDidChange;

@interface DDMotionManager : NSObject

@property UIDeviceOrientation deviceOrientation;
@property CGAffineTransform affineTransform;

+ (id)sharedMotionManager;
- (void)startAccelerometerUpdates:(NSTimeInterval)updateInterval;
- (void)stopAccelerometerUpdates;

@end
