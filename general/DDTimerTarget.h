//
//  DDTimerTarget.h
//  qianlieye
//
//  Created by darkdong on 14-3-12.
//  Copyright (c) 2014年 paixiu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DDTimerTarget : NSObject

+ (NSTimer *)scheduledTimerWithTimeInterval:(NSTimeInterval)timerInterval target:(id)target selector:(SEL)selector userInfo:(id)userInfo repeats:(BOOL)repeats;

@end
