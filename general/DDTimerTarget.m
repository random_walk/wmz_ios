//
//  DDTimerTarget.m
//  qianlieye
//
//  Created by darkdong on 14-3-12.
//  Copyright (c) 2014年 paixiu. All rights reserved.
//

#import "DDTimerTarget.h"

@interface DDTimerTarget ()
@property (weak) id target;
@property SEL selector;
@property NSTimer* timer;
@end

@implementation DDTimerTarget

+ (NSTimer *)scheduledTimerWithTimeInterval:(NSTimeInterval)timerInterval target:(id)target selector:(SEL)selector userInfo:(id)userInfo repeats:(BOOL)repeats {
    DDTimerTarget* timerTarget = [[self alloc] init];
    timerTarget.target = target;
    timerTarget.selector = selector;
    timerTarget.timer = [NSTimer scheduledTimerWithTimeInterval:timerInterval target:timerTarget selector:@selector(fire) userInfo:userInfo repeats:repeats];
    return timerTarget.timer;
}

//- (void)dealloc {
//    NSLog(@"DDTimerTarget dealloc");
//}

- (void)fire {
    if (self.target) {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
        [self.target performSelector:self.selector withObject:nil];
#pragma clang diagnostic pop
    }else {
        [self.timer invalidate];
    }
}

@end
