//
//  DDUtility.m
//  dd
//
//  Created by darkdong on 13-3-19.
//  Copyright (c) 2013年 darkdong. All rights reserved.
//

#import <ImageIO/ImageIO.h>
#import "DDUtility.h"
#include <sys/sysctl.h>
#include <sys/socket.h>
#include <ifaddrs.h>
#include <net/ethernet.h>
#include <net/if.h>
#include <net/if_dl.h>
#import "UIView+Additions.h"
#import "UIImage+Additions.h"
#import "DDGlobal.h"
#import "UIAlertView+Blocks.h"

NSString * const DDArrayMergingModeKeyMaxID = @"maxID";
NSString * const DDArrayMergingModeKeyMinID = @"minID";

static NSString *DUUserDefaultsKeyBitMask = @"BitMask";

#pragma mark - parse JSON

inline BOOL DUObjectIsString(id obj)
{
    return [obj isKindOfClass:[NSString class]];
}
inline BOOL DUObjectIsStringOrNumber(id obj)
{
    return [obj isKindOfClass:[NSString class]] || [obj isKindOfClass:[NSNumber class]];
}
inline BOOL DUObjectIsArray(id obj)
{
    return [obj isKindOfClass:[NSArray class]];
}
id DUJSONObjectWithData(NSData *data)
{
    if (![data isKindOfClass:[NSData class]]) {
        return nil;
    }
    return [NSJSONSerialization JSONObjectWithData:data options:0 error:NULL];
}

BOOL DUResponseObjectIsSuccess(id obj)
{
    if (![obj isKindOfClass:[NSDictionary class]]) {
        return NO;
    }
    NSNumber *resultObj = [obj objectForKey:@"result"];
    if (!resultObj) {
        return NO;
    }
    NSInteger result = [resultObj integerValue];
    return 0 == result;
}

#pragma mark - os info

CGFloat DUGetStatusBarHeight(void)
{
    return [[UIApplication sharedApplication] statusBarFrame].size.height;
}

BOOL DUDeviceIsLongScreen(void)
{
    static BOOL isLongScreen = NO;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        isLongScreen = 568 == [UIScreen mainScreen].bounds.size.height;
    });
    
    return isLongScreen;
}

NSArray * DUGetAllFontNames(void)
{
    NSMutableArray *marray = [NSMutableArray array];
    NSArray *familyNames = [UIFont familyNames];
    for (NSString *familyName in familyNames) {
        NSMutableDictionary *mdic = [NSMutableDictionary dictionary];
        NSArray *fontNames = [UIFont fontNamesForFamilyName:familyName];
        mdic[familyName] = fontNames;
        [marray addObject:mdic];
    }
    return [marray copy];
}

#pragma mark - app info

NSString * DUGetAppVersion(void)
{
    static NSString *appVersion = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSString *version = (__bridge NSString *)CFBundleGetValueForInfoDictionaryKey(CFBundleGetMainBundle(), (CFStringRef)@"CFBundleShortVersionString");
        appVersion = [[NSString alloc] initWithString:version];
    });
    
    return appVersion;
}

#pragma mark - dispatch queue

inline dispatch_queue_t DUDispatchGetMainQueue(void)
{
    return dispatch_get_main_queue();
}

inline dispatch_queue_t DUDispatchGetGlobalQueue(void)
{
    return dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
}

#pragma mark - string

NSString * DUGetUUID(void)
{
    CFUUIDRef uuidRef = CFUUIDCreate(kCFAllocatorDefault);
    CFStringRef stringRef = CFUUIDCreateString(kCFAllocatorDefault, uuidRef);
    CFRelease(uuidRef);
    NSString *uuid = CFBridgingRelease(stringRef);
    return uuid;
}

NSString * DUGetStringFromFourCharCode(UInt32 fourCharCode)
{
    if (0 == fourCharCode) {
        return @"";
    }
    UInt32 beFormatID = CFSwapInt32HostToBig(fourCharCode);
    return [[NSString alloc] initWithBytes:&beFormatID length:sizeof(beFormatID) encoding:NSASCIIStringEncoding];
}

BOOL DUVersionGreaterOrEqualTo(NSString *version1, NSString *version2)
{
    return NSOrderedAscending != [version1 compare:version2 options:NSNumericSearch];
}

BOOL DUVersionLessThan(NSString *version1, NSString *version2)
{
    return NSOrderedAscending == [version1 compare:version2 options:NSNumericSearch];
}

BOOL DUSystemVersionGreatOrEqualTo(NSString *version)
{
    NSString *systemVersion = [[UIDevice currentDevice] systemVersion];
    return DUVersionGreaterOrEqualTo(systemVersion, version);
}

#pragma mark - color

UIColor* DUGetTransparentColorForClick(void)
{
    return RGBACOLOR(0, 0, 0, 0.01);
}

#pragma mark - file system

NSArray * DUFSGetOrderedFiles(NSString *directory, BOOL excludingHiddenFiles)
{
    NSError *error = nil;
    NSArray *files = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:directory error:&error];
    if (error) {
        return nil;
    }
    NSMutableArray *mfiles = [files mutableCopy];
    if (excludingHiddenFiles) {
        NSIndexSet *indexes = [mfiles indexesOfObjectsPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
            return [obj hasPrefix:@"."];
        }];
        [mfiles removeObjectsAtIndexes:indexes];
    }
    [mfiles sortUsingSelector:@selector(caseInsensitiveCompare:)];
    return [mfiles copy];
}

NSString * DUFSGetNextOrderPlistFilePath(NSString *directory)
{
    BOOL excludingHiddenFiles = YES;
    NSArray *files = DUFSGetOrderedFiles(directory, excludingHiddenFiles);
    NSString *fileName = [files lastObject];
    NSUInteger fileOrder = [[[fileName lastPathComponent] stringByDeletingPathExtension] integerValue];
    if (fileOrder >= 99999) {
        fileOrder = 1;
    }else {
        fileOrder++;
    }
    //00001-99999
    NSString *filePath = [directory stringByAppendingPathComponent:[NSString stringWithFormat:@"%05u", fileOrder]];
    return [filePath stringByAppendingPathExtension:@"plist"];
}

void DUFSDeleteAllFilesInDirectory(NSString *directory)
{
    NSLog(@"DUFSDeleteAllFilesInDirectory %@", directory);
    NSFileManager *defaultManager = [NSFileManager defaultManager];
    NSArray *files = [defaultManager contentsOfDirectoryAtPath:directory error:NULL];
    for (NSString *fileName in files) {
        NSString *filePath = [directory stringByAppendingPathComponent:fileName];
        [defaultManager removeItemAtPath:filePath error:NULL];
    }
}

unsigned long long DUFSGetDirectorySize(NSString *directory)
{
    unsigned long long int folderSize = 0;

    NSFileManager *defaultManager = [NSFileManager defaultManager];
    NSArray *files = [defaultManager contentsOfDirectoryAtPath:directory error:nil];
    
    for (NSString *fileName in files) {
        NSString *filePath = [directory stringByAppendingPathComponent:fileName];
        NSDictionary *fileAttributes = [defaultManager attributesOfItemAtPath:filePath error:nil];
        unsigned long long fileSize = [fileAttributes[NSFileSize] unsignedLongLongValue];
        folderSize += fileSize;
    }
    return folderSize;
}

NSString * DUFSGetSizeString(unsigned long long size)
{
    if (size > 1024 * 1024) {
        unsigned long long mb = size / (1024 * 1024);
        return [NSString stringWithFormat:@"%llu M", mb];
    }else {
        unsigned long long kb = size / 1024;
        return [NSString stringWithFormat:@"%llu K", kb];
    }
}

#pragma mark - merging array

static NSDictionary * DUMakeModelMergingInfo(id model, SEL comparator, NSString *key)
{
    if ([model respondsToSelector:comparator]) {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
        NSNumber *number = [model performSelector:comparator];
#pragma clang diagnostic pop
        if (number && key.length > 0) {
            return @{key: number};
        }
    }
    return nil;
}

NSDictionary * DUMakeArrayMergingModeInfo(NSArray *models, DDArrayMergingMode mode, SEL comparator, NSDictionary *customKeyInfo)
{
    if (0 == models.count) {
        return @{};
    }
    
    if(DDArrayMergingModeAppend == mode) {
        id model = models.lastObject;
        NSString *key = customKeyInfo[DDArrayMergingModeKeyMaxID];
        NSDictionary *info = DUMakeModelMergingInfo(model, comparator, key);
        if (info) {
            return info;
        }
    }else if(DDArrayMergingModePreappend == mode) {
        NSString *key = customKeyInfo[DDArrayMergingModeKeyMinID];
        for (id model in models) {
            NSDictionary *info = DUMakeModelMergingInfo(model, comparator, key);
            if (info) {
                return info;
            }
        }
    }
    return @{};
}

#pragma mark - AVFoundation

AVCaptureConnection * DUCaptureSessionGetConnection(AVCaptureSession *session, NSString *mediaType)
{
    for (AVCaptureOutput *output in session.outputs) {
        AVCaptureConnection *connection = DUCaptureOutputGetConnection(output, mediaType);
        if (connection) {
            return connection;
        }
    }
    return nil;
}

AVCaptureConnection * DUCaptureOutputGetConnection(AVCaptureOutput *output, NSString *mediaType)
{
    for (AVCaptureConnection *connection in output.connections) {
        for (AVCaptureInputPort *inputPort in connection.inputPorts) {
            if ([inputPort.mediaType isEqualToString:mediaType]) {
                return connection;
            }
        }
    }
    return nil;
}

AVCaptureDeviceInput * DUCaptureSessionGetDeviceInput(AVCaptureSession *session, NSString *mediaType)
{
    for (AVCaptureInput *input in session.inputs) {
        if ([input isKindOfClass:[AVCaptureDeviceInput class]]) {
            AVCaptureDeviceInput *deviceInput = (AVCaptureDeviceInput *)input;
            AVCaptureDevice *device = deviceInput.device;
            if (device.connected && [device hasMediaType:mediaType]) {
                return deviceInput;
            }
        }
    }
    return nil;
}

AVCaptureOutput * DUCaptureSessionGetOutputByClass(AVCaptureSession *session, Class outputClass)
{
    for (AVCaptureOutput *output in session.outputs) {
        if ([output isKindOfClass:outputClass]) {
            return output;
        }
    }
    return nil;
}

AVCaptureOutput * DUCaptureSessionGetOutput(AVCaptureSession *session, NSString *mediaType)
{
    for (AVCaptureOutput *output in session.outputs) {
        //        AVCaptureConnection *connection = [output connectionWithMediaType:mediaType];
        //        if (connection) {
        //            return output;
        //        }
        for (AVCaptureConnection *connection in output.connections) {
            for (AVCaptureInputPort *inputPort in connection.inputPorts) {
                if ([inputPort.mediaType isEqualToString:mediaType]) {
                    return output;
                }
            }
        }
    }
    return nil;
}

AVCaptureDevice * DUCaptureDeviceGetCamera(AVCaptureDevicePosition position)
{
    NSArray *devices = [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo];
    for (AVCaptureDevice *device in devices) {
        if ([device position] == position) {
            return device;
        }
    }
    return nil;
}

AVCaptureDevicePosition DUCaptureDeviceGetCameraPosition(AVCaptureSession *session)
{
    AVCaptureDeviceInput *videoInput = DUCaptureSessionGetDeviceInput(session, AVMediaTypeVideo);
    if (!videoInput) {
        return AVCaptureDevicePositionBack;
    }
    
    return videoInput.device.position;
}

NSUInteger DUCaptureDeviceGetCameraCount(void)
{
    return [[AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo] count];
}

void DUPlayWithVolume(AVPlayer *player, float volume)
{
    NSArray *audioTracks = [player.currentItem.asset tracksWithMediaType:AVMediaTypeAudio];
    
    // Mute all the audio tracks
    NSMutableArray *allAudioParams = [NSMutableArray array];
    for (AVAssetTrack *track in audioTracks) {
        AVMutableAudioMixInputParameters *audioInputParams = [AVMutableAudioMixInputParameters audioMixInputParameters];
        [audioInputParams setVolume:volume atTime:kCMTimeZero];
        [audioInputParams setTrackID:[track trackID]];
        [allAudioParams addObject:audioInputParams];
    }
    AVMutableAudioMix *audioZeroMix = [AVMutableAudioMix audioMix];
    [audioZeroMix setInputParameters:allAudioParams];
    
    [player.currentItem setAudioMix:audioZeroMix];
}

void DUPlayInSilentMode(void)
{
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    NSError *error = nil;
    BOOL result = NO;
    if ([audioSession respondsToSelector:@selector(setActive:withOptions:error:)]) {
        result = [audioSession setActive:YES withOptions:0 error:&error]; // iOS6+
    } else {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
        [audioSession setActive:YES withFlags:0 error:&error]; // iOS5 and below
#pragma clang diagnostic pop
    }
    if (!result && error) {
        // deal with the error
    }
    
    error = nil;
    result = [audioSession setCategory:AVAudioSessionCategoryPlayback error:&error];
    
    if (!result && error) {
        // deal with the error
    }
}

UIImage * DUSampleBufferGetImage(CMSampleBufferRef sampleBuffer, AVCaptureDevicePosition devicePosition)
{
    if (!sampleBuffer) {
        return nil;
    }
    
    UIImageOrientation imageOrientation = (AVCaptureDevicePositionFront == devicePosition)? UIImageOrientationLeftMirrored: UIImageOrientationRight;
    
    // Get a CMSampleBuffer's Core Video image buffer for the media data
    CVImageBufferRef imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer);
    // Lock the base address of the pixel buffer
    CVPixelBufferLockBaseAddress(imageBuffer, 0);
    
    // Get the number of bytes per row for the pixel buffer
    void *baseAddress = CVPixelBufferGetBaseAddress(imageBuffer);
    
    // Get the number of bytes per row for the pixel buffer
    size_t bytesPerRow = CVPixelBufferGetBytesPerRow(imageBuffer);
    // Get the pixel buffer width and height
    size_t width = CVPixelBufferGetWidth(imageBuffer);
    size_t height = CVPixelBufferGetHeight(imageBuffer);
    
    // Create a device-dependent RGB color space
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    
    // Create a bitmap graphics context with the sample buffer data
    CGContextRef context = CGBitmapContextCreate(baseAddress, width, height, 8,
                                                 bytesPerRow, colorSpace, kCGBitmapByteOrder32Little | kCGImageAlphaPremultipliedFirst);
    // Create a Quartz image from the pixel data in the bitmap graphics context
    CGImageRef quartzImage = CGBitmapContextCreateImage(context);
    // Unlock the pixel buffer
    CVPixelBufferUnlockBaseAddress(imageBuffer,0);
    
    // Free up the context and color space
    CGContextRelease(context);
    CGColorSpaceRelease(colorSpace);
    
    // Create an image object from the Quartz image
    UIImage *image = [UIImage imageWithCGImage:quartzImage scale:1.0 orientation:imageOrientation];
    
    // Release the Quartz image
    CGImageRelease(quartzImage);
    
    return (image);
}

CGPoint DUCameraConvertPointOfInterestToView(CGPoint pointOfInterest, AVCaptureVideoPreviewLayer *videoPreviewLayer, AVCaptureSession *session, AVCaptureOutput *output)
{
    //PXLog(@"convertPointOfInterest %@ %@", NSStringFromCGPoint(pointOfInterest), NSStringFromCGSize(size));
    CGSize size = videoPreviewLayer.frame.size;
    NSString *videoGravity = videoPreviewLayer.videoGravity;
    CGPoint pointOfView = CGPointZero;
    if ([videoGravity isEqualToString:AVLayerVideoGravityResize] ) {
		// Scale, switch x and y, and reverse x
        pointOfView = CGPointMake((1 - pointOfInterest.y) * size.width, pointOfInterest.x * size.height);
    } else {
        CGRect cleanAperture;
        for (AVCaptureInput *input in session.inputs) {
            for (AVCaptureInputPort *inputPort in input.ports) {
                if ([inputPort.mediaType isEqualToString:AVMediaTypeVideo]) {
                    cleanAperture = CMVideoFormatDescriptionGetCleanAperture(inputPort.formatDescription, YES);
                    CGSize apertureSize = cleanAperture.size;
                    if (0 == apertureSize.height || 0 == apertureSize.width) {
                        apertureSize.width = 2 * size.height;
                        apertureSize.height = 2 * size.width;
                    }
                    CGFloat apertureRatio = apertureSize.height / apertureSize.width;
                    CGFloat viewRatio = size.width / size.height;
                    
                    if ([videoGravity isEqualToString:AVLayerVideoGravityResizeAspect] ) {
                    } else if ([videoGravity isEqualToString:AVLayerVideoGravityResizeAspectFill]) {
                        // Scale, switch x and y, and reverse x
                        if (viewRatio > apertureRatio) {
                            CGFloat y2 = apertureSize.width * (size.width / apertureSize.height);
                            pointOfView.x = (1 - pointOfInterest.y) * size.width;
                            pointOfView.y = pointOfInterest.x * y2 - ((y2 - size.height)/2.0);
                        } else {
                            CGFloat x2 = apertureSize.height * (size.height / apertureSize.width);
                            pointOfView.x = (1.0 - pointOfInterest.y) * x2 - (x2 - size.width)/2.0;
                            pointOfView.y = pointOfInterest.x * size.height;
                        }
                    }
                    break;
                }
            }
        }
    }
    
    AVCaptureConnection *connection = DUCaptureOutputGetConnection(output, AVMediaTypeVideo);
    if (connection.isVideoMirrored) {
        pointOfView.x = size.width - pointOfView.x;
    }
    //    DDLog(@"pointOfInterest %@ is converted to pointOfView %@", NSStringFromCGPoint(pointOfInterest), NSStringFromCGPoint(pointOfView));
    return pointOfView;
}

CGPoint DUCameraConvertPointOfViewToInterest(CGPoint pointOfView, AVCaptureVideoPreviewLayer *videoPreviewLayer, AVCaptureSession *session, AVCaptureOutput *output)
{
    CGPoint pointOfInterest = CGPointMake(0.5, 0.5);
    CGSize size = videoPreviewLayer.frame.size;
    NSString *videoGravity = videoPreviewLayer.videoGravity;
    
    AVCaptureConnection *connection = DUCaptureOutputGetConnection(output, AVMediaTypeVideo);
    if (connection.isVideoMirrored) {
        pointOfView.x = size.width - pointOfView.x;
    }
    
    if ( [videoGravity isEqualToString:AVLayerVideoGravityResize] ) {
		// Scale, switch x and y, and reverse x
        pointOfInterest = CGPointMake(pointOfView.y / size.height, 1.f - (pointOfView.x / size.width));
    } else {
        CGRect cleanAperture;
        for (AVCaptureInput *input in session.inputs) {
            for (AVCaptureInputPort *inputPort in input.ports) {
                if ([inputPort.mediaType isEqualToString:AVMediaTypeVideo]) {
                    cleanAperture = CMVideoFormatDescriptionGetCleanAperture(inputPort.formatDescription, YES);
                    CGSize apertureSize = cleanAperture.size;
                    CGPoint point = pointOfView;
                    if (0 == apertureSize.height || 0 == apertureSize.width) {
                        apertureSize.width = 2 * size.height;
                        apertureSize.height = 2 * size .width;
                    }
                    CGFloat apertureRatio = apertureSize.height / apertureSize.width;
                    CGFloat viewRatio = size.width / size.height;
                    CGFloat xc = .5f;
                    CGFloat yc = .5f;
                    
                    if ( [videoGravity isEqualToString:AVLayerVideoGravityResizeAspect] ) {
                        if (viewRatio > apertureRatio) {
                            CGFloat y2 = size.height;
                            CGFloat x2 = size.height * apertureRatio;
                            CGFloat x1 = size.width;
                            CGFloat blackBar = (x1 - x2) / 2;
                            // If point is inside letterboxed area, do coordinate conversion; otherwise, don't change the default value returned (.5,.5)
                            if (point.x >= blackBar && point.x <= blackBar + x2) {
                                // Scale (accounting for the letterboxing on the left and right of the video preview), switch x and y, and reverse x
                                xc = point.y / y2;
                                yc = 1.f - ((point.x - blackBar) / x2);
                            }
                        } else {
                            CGFloat y2 = size.width / apertureRatio;
                            CGFloat y1 = size.height;
                            CGFloat x2 = size.width;
                            CGFloat blackBar = (y1 - y2) / 2;
                            // If point is inside letterboxed area, do coordinate conversion. Otherwise, don't change the default value returned (.5,.5)
                            if (point.y >= blackBar && point.y <= blackBar + y2) {
                                // Scale (accounting for the letterboxing on the top and bottom of the video preview), switch x and y, and reverse x
                                xc = ((point.y - blackBar) / y2);
                                yc = 1.f - (point.x / x2);
                            }
                        }
                    } else if ([videoGravity isEqualToString:AVLayerVideoGravityResizeAspectFill]) {
                        // Scale, switch x and y, and reverse x
                        if (viewRatio > apertureRatio) {
                            CGFloat y2 = apertureSize.width * (size.width / apertureSize.height);
                            xc = (point.y + ((y2 - size.height) / 2.f)) / y2; // Account for cropped height
                            yc = (size.width - point.x) / size.width;
                        } else {
                            CGFloat x2 = apertureSize.height * (size.height / apertureSize.width);
                            yc = 1.f - ((point.x + ((x2 - size.width) / 2)) / x2); // Account for cropped width
                            xc = point.y / size.height;
                        }
                    }
                    pointOfInterest = CGPointMake(xc, yc);
                    break;
                }
            }
        }
    }
    
    //    DDLog(@"pointOfView %@ is converted to pointOfInterest %@", NSStringFromCGPoint(pointOfView), NSStringFromCGPoint(pointOfInterest));
    return pointOfInterest;
}

void DUCameraSetFocus(AVCaptureDevice *device, CGPoint pointOfInterest, AVCaptureFocusMode focusMode)
{
    if ([device isFocusPointOfInterestSupported] && [device isFocusModeSupported:focusMode]) {
        if ([device lockForConfiguration:NULL]) {
            device.focusPointOfInterest = pointOfInterest;
            device.focusMode = focusMode;
            [device unlockForConfiguration];
        }
    }
}

void DUCameraSetExposure(AVCaptureDevice *device, CGPoint pointOfInterest, AVCaptureExposureMode exposureMode)
{
    if (device.isExposurePointOfInterestSupported && [device isExposureModeSupported:exposureMode]) {
        if ([device lockForConfiguration:NULL]) {
            device.exposurePointOfInterest = pointOfInterest;
            device.exposureMode = exposureMode;
            [device unlockForConfiguration];
        }
    }
}

void DUCameraSetFlashMode(AVCaptureDevice *device, AVCaptureFlashMode flashMode)
{
    if (!device.hasFlash || ![device isFlashModeSupported:flashMode]) {
        return;
    }
    
    if ([device lockForConfiguration:NULL]) {
        device.flashMode = flashMode;
        [device unlockForConfiguration];
    }
}

void DUCameraSetTorchMode(AVCaptureDevice *device, AVCaptureTorchMode torchMode)
{
    if (!device.hasTorch || ![device isTorchModeSupported:torchMode]) {
        return;
    }
    
    if ([device lockForConfiguration:NULL]) {
        device.torchMode = torchMode;
        [device unlockForConfiguration];
    }
}

void DUCameraLockAEAF(AVCaptureDevice *device) {
    if ([device isFocusModeSupported:AVCaptureFocusModeLocked]) {
        if ([device lockForConfiguration:NULL]) {
            [device setFocusMode:AVCaptureFocusModeLocked];
            [device unlockForConfiguration];
        }
    }
    
    if ([device isExposureModeSupported:AVCaptureExposureModeLocked]) {
        if ([device lockForConfiguration:NULL]) {
            [device setExposureMode:AVCaptureExposureModeLocked];
            [device unlockForConfiguration];
        }
    }
}

#pragma mark - photo

void DUPhotoAddGPSInfo(CLLocation *location, NSMutableDictionary *metaData)
{
    if (!location) {
        return;
    }
    if (-[location.timestamp timeIntervalSinceNow] > 60 * 60) {
        return;
    }
    NSMutableDictionary *gps = [NSMutableDictionary dictionary];
    
    // GPS tag version
    [gps setObject:@"2.2.0.0" forKey:(NSString *)kCGImagePropertyGPSVersion];
    
    // Time and date must be provided as strings, not as an NSDate object
    NSDate *date = location.timestamp;
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"HH:mm:ss.SSSSSS"];
    [formatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    [gps setObject:[formatter stringFromDate:date] forKey:(NSString *)kCGImagePropertyGPSTimeStamp];
    [formatter setDateFormat:@"yyyy:MM:dd"];
    [gps setObject:[formatter stringFromDate:date] forKey:(NSString *)kCGImagePropertyGPSDateStamp];
    
    // Latitude
    CLLocationDegrees latitude = location.coordinate.latitude;
    if (latitude < 0) {
        latitude = -latitude;
        [gps setObject:@"S" forKey:(NSString *)kCGImagePropertyGPSLatitudeRef];
    } else {
        [gps setObject:@"N" forKey:(NSString *)kCGImagePropertyGPSLatitudeRef];
    }
    [gps setObject:[NSNumber numberWithFloat:latitude] forKey:(NSString *)kCGImagePropertyGPSLatitude];
    
    // Longitude
    CLLocationDegrees longitude = location.coordinate.longitude;
    if (longitude < 0) {
        longitude = -longitude;
        [gps setObject:@"W" forKey:(NSString *)kCGImagePropertyGPSLongitudeRef];
    } else {
        [gps setObject:@"E" forKey:(NSString *)kCGImagePropertyGPSLongitudeRef];
    }
    [gps setObject:[NSNumber numberWithFloat:longitude] forKey:(NSString *)kCGImagePropertyGPSLongitude];
    
    // Altitude
    CLLocationDistance altitude = location.altitude;
    if (!isnan(altitude)){
        if (altitude < 0) {
            altitude = -altitude;
            [gps setObject:@"1" forKey:(NSString *)kCGImagePropertyGPSAltitudeRef];
        } else {
            [gps setObject:@"0" forKey:(NSString *)kCGImagePropertyGPSAltitudeRef];
        }
        [gps setObject:[NSNumber numberWithFloat:altitude] forKey:(NSString *)kCGImagePropertyGPSAltitude];
    }
    
    // Speed, must be converted from m/s to km/h
    CLLocationSpeed speed = location.speed;
    if (speed >= 0){
        [gps setObject:@"K" forKey:(NSString *)kCGImagePropertyGPSSpeedRef];
        [gps setObject:[NSNumber numberWithFloat:speed * 3.6] forKey:(NSString *)kCGImagePropertyGPSSpeed];
    }
    
    // Heading
    CLLocationDirection course = location.course;
    if (course >= 0){
        [gps setObject:@"T" forKey:(NSString *)kCGImagePropertyGPSTrackRef];
        [gps setObject:[NSNumber numberWithFloat:course] forKey:(NSString *)kCGImagePropertyGPSTrack];
    }
    [metaData setObject:gps forKey:(NSString*)kCGImagePropertyGPSDictionary];
}

void DUPhotoAddImageOrientation(UIImageOrientation imageOrientation, NSMutableDictionary *metaData)
{
    //UIImageOrientationUp:             1
    //UIImageOrientationDown:           3
    //UIImageOrientationLeft:           8
    //UIImageOrientationRight:          6
    //UIImageOrientationUpMirrored:     2
    //UIImageOrientationDownMirrored:   4
    //UIImageOrientationLeftMirrored:   5
    //UIImageOrientationRightMirrored:  7
    
    NSArray *imgOri2PropOri = [NSArray arrayWithObjects:
                               [NSNumber numberWithInt:1],
                               [NSNumber numberWithInt:3],
                               [NSNumber numberWithInt:8],
                               [NSNumber numberWithInt:6],
                               nil];
    if (imageOrientation < 4) {
        [metaData setObject:[imgOri2PropOri objectAtIndex:imageOrientation] forKey:(NSString*)kCGImagePropertyOrientation];
    }else {
        [metaData setObject:[NSNumber numberWithInt:1] forKey:(NSString*)kCGImagePropertyOrientation];
    }
}

#pragma mark - user defaults
//按位的布尔值
NSUInteger DUUserDefaultsGetAllBitMask(void)
{
    return [[NSUserDefaults standardUserDefaults] integerForKey:DUUserDefaultsKeyBitMask];
}

void DUUserDefaultsSetAllBitMask(NSUInteger allBitMask)
{
    [[NSUserDefaults standardUserDefaults] setInteger:allBitMask forKey:DUUserDefaultsKeyBitMask];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

BOOL DUUserDefaultsHasBitMask(NSUInteger bitMask)
{
    NSUInteger allBitMask = DUUserDefaultsGetAllBitMask();
    return allBitMask & bitMask? YES: NO;
}

void DUUserDefaultsSetBitMask(NSUInteger bitMask, BOOL flag)
{
    NSInteger allBitMask = DUUserDefaultsGetAllBitMask();
    if (flag) {
        allBitMask |= bitMask;
    }else {
        allBitMask &= ~bitMask;
    }
    DUUserDefaultsSetAllBitMask(allBitMask);
}

#pragma mark - view

void DUShowPopupView(UIView *popupView, float delta, float duration, DDBlockVoid completion)
{    
    popupView.alpha = 0;
    popupView.top += delta;
    
    [UIView animateWithDuration:duration animations:^{
        popupView.alpha = 1;
        popupView.top -= delta;
    } completion:^(BOOL finished) {
        if (completion) {
            completion();
        }
    }];
}

void DUDismissPopupView(UIView *popupView, float delta, float duration, DDBlockVoid completion)
{
    [UIView animateWithDuration:duration animations:^{
        popupView.alpha = 0;
        popupView.top += delta;
    } completion:^(BOOL finished) {
        if(completion) {
            completion();
        }
    }];
}

BOOL DUScrollViewIsCloseToBottom(UIScrollView *scrollView)
{
    return CGRectGetMaxY(scrollView.bounds) + scrollView.contentSize.height / 3 > scrollView.contentSize.height;
}

UIButton* DUMakeStretchableImageButton(NSString *imageName, NSString *highlightedImageName)
{
    UIImage *image = [UIImage stretchableImageNamedNoCache:imageName];
    UIImage *highlightedImage = [UIImage stretchableImageNamedNoCache:highlightedImageName];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setBackgroundImage:image forState:UIControlStateNormal];
    [button setBackgroundImage:highlightedImage forState:UIControlStateHighlighted];
    [button sizeToFit];
    return button;
}

UIWindow* DUGetMainWindow(void)
{
    id delegate = [[UIApplication sharedApplication] delegate];
    return [delegate window];
}

void DUShowSharedAlertView(NSString *title, NSString *message, NSString *okLabel)
{
    static BOOL alertViewIsShowing = NO;
    
    alertViewIsShowing = YES;
    
    RIButtonItem *okItem = [RIButtonItem itemWithLabel:okLabel];
    okItem.action = ^{
        alertViewIsShowing = NO;
    };
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                    message:message
                                           cancelButtonItem:nil
                                           otherButtonItems:okItem, nil];
    [alert show];
}
//BOOL DUAlertViewExists(void)
//{
//    if (!DUSystemVersionGreatOrEqualTo(@"7.0")) {
//        UIAlertView *topMostAlert = [NSClassFromString(@"_UIAlertManager") performSelector:@selector(topMostAlert)];
//        return !!topMostAlert;
//    }else {
//        NSArray *windows = [[UIApplication sharedApplication] windows];
//        if (windows.count <= 1) {
//            return NO;
//        }
//        
//        UIWindow *window = windows[1];
//        NSArray *views = window.subviews;
//        if (views.count > 0) {
//            UIView *view = views[0];
//            BOOL hasAlert = [view isKindOfClass:[UIAlertView class]];
//            BOOL hasSheet = [view isKindOfClass:[UIActionSheet class]];
//            return hasAlert || hasSheet;
//        }
//        return NO;
//    }
//}
#pragma mark - controller

id DUGetMainController(void)
{
    UIWindow *window = DUGetMainWindow();
    UIViewController *rootController = window.rootViewController;
    if ([rootController isKindOfClass:[UINavigationController class]]) {
        UINavigationController *navigationController = (UINavigationController *)rootController;
        if (navigationController.viewControllers.count > 0) {
            return navigationController.viewControllers[0];
        }
    }
    return nil;
}

@implementation DDUtility

+ (NSMutableSet *)monitor {
	static NSMutableSet *monitor = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        monitor = [[NSMutableSet alloc] init];
    });
    return monitor;
}

+ (BOOL)monitorContainsObject:(NSUInteger)objectHashCode {
    return [self.monitor containsObject:@(objectHashCode)];
}

+ (void)monitorAddObject:(NSUInteger)objectHashCode {
    [self.monitor addObject:@(objectHashCode)];
}

+ (void)monitorRemoveObject:(NSUInteger)objectHashCode {
    [self.monitor removeObject:@(objectHashCode)];
}


@end
