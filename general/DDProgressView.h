//
//  DDProgressView.h
//  dd
//
//  Created by darkdong on 13-11-28.
//  Copyright (c) 2013年 PixShow. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DDProgressView : UIView
{
    CGFloat _animationCurrentProgress;
}

@property (nonatomic) CGFloat progress;

- (void)invalidateTimer;
- (void)setProgress:(CGFloat)progress animated:(BOOL)animated;
- (void)setProgressAnimately:(CGFloat)progress animationDuration:(NSTimeInterval)animationDuration;

@end
