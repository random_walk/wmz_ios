//
//  DDSharedPlayer.m
//  dd
//
//  Created by darkdong on 13-6-4.
//  Copyright (c) 2013年 darkdong. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>
#import "DDSharedPlayer.h"
#import "DDPreprocessMacro.h"
#import "DDUtility.h"

//static NSString *DDSharedPlayerKVOKeyPath = @"status";

static NSURL * GetURLFromPlayerItem(AVPlayerItem *playerItem)
{
    AVAsset *asset = playerItem.asset;
    if ([asset isKindOfClass:AVURLAsset.class]) {
        return [(AVURLAsset *)asset URL];
    }else {
        return nil;
    }
}

@interface DDSharedPlayer ()

@end

@implementation DDSharedPlayer

+ (DDSharedPlayer *)player {
    static DDSharedPlayer *sharedPlayer = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedPlayer = [[DDSharedPlayer alloc] init];
        
    });
    
    return sharedPlayer;
}

- (id)init {
    self = [super init];
    if (self) {
        self.avPlayer = [[AVPlayer alloc] init];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playerItemDidPlayToEndTime:)
                                                     name:AVPlayerItemDidPlayToEndTimeNotification
                                                   object:nil];
    }
    return self;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)play {
#if (TARGET_IPHONE_SIMULATOR)
    return;
#endif
    AVPlayerItem *currentItem = self.avPlayer.currentItem;
    NSURL *currentURL = GetURLFromPlayerItem(currentItem);
    DDLog(@"currentItem %@ currentURL %@ url %@", currentItem, currentURL, self.url);

    if (![self.url isEqual:currentURL]) {
        DDLog(@"play new item");
        //当前的item复位
        
        AVPlayerItem *item = [[AVPlayerItem alloc] initWithURL:self.url];
        [self.avPlayer replaceCurrentItemWithPlayerItem:item];
        self.avPlayer.rate = 0;        
    }
    
    DDLog(@"player status %d", self.avPlayer.currentItem.status);
    
    DDLog(@"player rate %f", self.avPlayer.rate);
    if (0 == self.avPlayer.rate) {
        [self.avPlayer play];
    }else {
        [self.avPlayer pause];
    }
    DDLog(@"player rate now %f", self.avPlayer.rate);
}

- (void)pause {
    [self.avPlayer pause];
}

- (void)clear {
    self.url = nil;
    [self.avPlayer replaceCurrentItemWithPlayerItem:nil];
    self.avPlayer.rate = 0;
}

- (BOOL)isPlaying {
    return 1 == self.avPlayer.rate;
}

//AVPlayer notification
- (void)playerItemDidPlayToEndTime:(NSNotification *)notification {
    DDLog(@"playerItemDidPlayToEndTime %@", notification);
    AVPlayerItem *playerItem = notification.object;
    if (playerItem != self.avPlayer.currentItem) {
        //不是我们的通知
        DDLog(@"noti is not ours");
        return;
    }
    [playerItem seekToTime:kCMTimeZero];
    if (self.loopingPlay) {
        DDLog(@"play again");
        //稍等一会儿再播放，否则状态不对
        double delayInSeconds = 0.5;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            [self play];
        });
    }
}

@end
