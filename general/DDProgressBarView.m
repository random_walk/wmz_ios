//
//  DDProgressBarView.m
//  dd
//
//  Created by darkdong on 14-3-5.
//  Copyright (c) 2014年 paixiu. All rights reserved.
//

#import "DDProgressBarView.h"
#import "DDProgressView_Redeclare.h"
#import "UIView+Additions.h"

@implementation DDProgressBarView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.backgroundColor = [UIColor clearColor];
        self.cornerRadius = 0;
        self.progressColor = [UIColor redColor];
    }
    return self;
}

- (void)setFrame:(CGRect)frame
{
    [super setFrame:frame];
    [self makeShapeMask];
}

- (void)setCornerRadius:(CGFloat)cornerRadius
{
    _cornerRadius = cornerRadius;
    [self makeShapeMask];
}

- (void)drawRect:(CGRect)rect
{
    // Drawing code
    CGContextRef context = UIGraphicsGetCurrentContext();

    CGRect fullRect = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
    CGFloat delta = 0.5;
    CGRect progressRect = CGRectInset(fullRect, delta, delta);
    progressRect.size.width *= self.animationCurrentProgress;
    
    UIBezierPath *progressPath;
    if (self.cornerRadius < delta) {
        progressPath = [UIBezierPath bezierPathWithRect:progressRect];
    }else {
        progressPath = [UIBezierPath bezierPathWithRoundedRect:progressRect cornerRadius:self.cornerRadius - delta];
    }
    
    //进度条，填充，描边
    CGContextAddPath(context, progressPath.CGPath);
    CGContextSetFillColorWithColor(context, self.progressColor.CGColor);
    CGContextDrawPath(context, kCGPathFill);
}

#pragma mark - private

- (void)makeShapeMask
{
    CGRect rect = CGRectMake(0, 0, self.width, self.height);
    UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:rect cornerRadius:self.cornerRadius];
    CAShapeLayer *shapeLayer = [CAShapeLayer layer];
    shapeLayer.path = path.CGPath;
    shapeLayer.cornerRadius = self.cornerRadius;
    self.layer.mask = shapeLayer;
}

@end
