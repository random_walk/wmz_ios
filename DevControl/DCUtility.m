//
//  DCUtility.m
//  DevControl
//
//  Created by rumble on 14/7/25.
//  Copyright (c) 2014年 WMZ. All rights reserved.
//

#import "DCUtility.h"
#import "UIImage+Additions.h"
#import "AppDelegate.h"
#import "UIView+Additions.h"

UIButton *DCGetCommonButton()
{
    UIButton *btn = [UIButton new];
    [btn setBackgroundImage:[UIImage imageNamedNoCache:@"btn_circle"] forState:UIControlStateNormal];
    [btn setBackgroundImage:[UIImage imageNamedNoCache:@"btn_circle_highlight"] forState:UIControlStateHighlighted];
    [btn setBackgroundImage:[UIImage imageNamedNoCache:@"btn_circle_pressed"] forState:UIControlStateSelected];
    [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor redColor] forState:UIControlStateSelected];
    
    [btn sizeToFit];
    
    if(DCIsIpad()) {
        //Device is ipad
    }else{
        btn.size = CGSizeMake(70, 70);
        btn.titleLabel.font = [UIFont systemFontOfSize:14];
    }
    
    return btn;
}

UILabel *DCGetCommonLabel()
{
    UILabel *lab = [UILabel new];
    lab.backgroundColor = [UIColor clearColor];
    lab.textColor = [UIColor blackColor];
    CGFloat fontsize = DCIsIpad()? 18: 14;
    lab.font = [UIFont systemFontOfSize:fontsize];
    
    return lab;
}

void DCAlertWithTitleAndMessage(NSString *title, NSString *message)
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title message:message delegate:nil cancelButtonTitle:@"知道了" otherButtonTitles:nil, nil];
    
    [alertView show];
}

GCDAsyncSocket *DCGetSocket()
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    return [appDelegate getAsyncSocket];
}

BOOL DCConnect2Server()
{
    GCDAsyncSocket *socket = DCGetSocket();
    
    NSString *host = HOST;
    uint16_t port = PORT;
    
    NSError *error = nil;
    if (![socket connectToHost:host onPort:port error:&error])
    {
        NSLog(@"Error connecting: %@", error);
        return NO;
    }else {
        return YES;
    }
}

void DCDisconnect()
{
    GCDAsyncSocket *socket = DCGetSocket();
    
    [socket disconnect];
}

void DCSendCommand2Server(NSString *message)
{
    GCDAsyncSocket *socket = DCGetSocket();
    
    NSData *requestData = [[NSString stringWithFormat:@"%@\r\n", message] dataUsingEncoding:NSUTF8StringEncoding];
    
    [socket writeData:requestData withTimeout:-1 tag:0];
    //[socket readDataToData:[GCDAsyncSocket CRLFData] withTimeout:-1 tag:0];
}

BOOL DCIsIpad()
{
    return [UIDevice currentDevice].userInterfaceIdiom==UIUserInterfaceIdiomPad;
}

@implementation DCUtility

@end
