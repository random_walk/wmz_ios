//
//  DCConstants.m
//  DevControl
//
//  Created by rumble on 14/7/25.
//  Copyright (c) 2014年 WMZ. All rights reserved.
//

#import "DCConstants.h"

NSString * const DCNotificationNameSocketDidConnect = @"DCNotificationNameSocketDidConnect";
NSString * const DCNotificationNameSocketDidDisconnect = @"DCNotificationNameSocketDidDisconnect";
NSString * const DCNotificationNameSocketGetDevList = @"DCNotificationNameSocketGetDevList";
NSString * const DCNotificationNameSocketGetDetail = @"DCNotificationNameSocketGetDetail";
NSString * const DCNotificationNameSocketGetCommond = @"DCNotificationNameSocketGetCommond";