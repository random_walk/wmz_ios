//
//  DCProgressView.m
//  DevControl
//
//  Created by rumble on 14/7/28.
//  Copyright (c) 2014年 WMZ. All rights reserved.
//

#import "DCProgressView.h"
#import "UIView+Additions.h"

@interface DCProgressView ()

@property (nonatomic, strong) NSArray   *unitViewArr;
@property (nonatomic, strong) UIColor   *selectedColor;
@property (nonatomic, strong) UIColor   *unSelctedColor;

@property (nonatomic) CGFloat       margin;
@property (nonatomic) NSInteger     unitsNum;
@end

@implementation DCProgressView

- (id)initWithFrame:(CGRect)frame unitsNum:(NSInteger)unitsNum
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        NSLog(@"hoho");
        [self setupUnitsWithNum:unitsNum];
        self.margin = 2;
        self.unitsNum = unitsNum;
        self.selectedColor = [UIColor greenColor];
        self.unSelctedColor = [UIColor lightGrayColor];
    }
    
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void)setProgressValue:(NSUInteger)progressValue
{
    _progressValue = progressValue;
    [self setNeedsLayout];
}

- (void)setMargin:(CGFloat)margin
{
    _margin = margin;
    
    [self setNeedsLayout];
}
- (void)setSelectedColor:(UIColor *)selectedColor
{
    _selectedColor = selectedColor;
    [self setNeedsLayout];
}
- (void)setUnSelectedColor:(UIColor *)unSelectedColor
{
    _unSelctedColor = unSelectedColor;
    [self setNeedsLayout];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    NSLog(@"unitsNum:%i, %@", _unitsNum, NSStringFromCGSize(self.size));
    if (_unitsNum == 0) {
        return;
    }
    CGSize unitSize = CGSizeMake((self.width - self.margin * (self.unitsNum - 1)) / self.unitsNum, self.height);
    int index = 0;
    for (UIView *unitView in self.unitViewArr) {
        
        unitView.frame = CGRectMake((unitSize.width + self.margin) * index, 0, unitSize.width, unitSize.height);
        
        if (unitView.tag > _progressValue) {
            unitView.backgroundColor = self.unSelctedColor;
        }else {
            unitView.backgroundColor = self.selectedColor;
        }
        
        index++;
    }
    
}

- (void)setupUnitsWithNum:(NSInteger)unitsNum
{
    
    CGSize unitSize = CGSizeMake((self.width - self.margin * (unitsNum - 1)) / unitsNum, self.height);
    NSMutableArray *tempArr = [NSMutableArray array];
    
    for (int i = 0; i < unitsNum; i++) {
        
        UIView  *unitView = [[UIView alloc] initWithFrame:CGRectMake((unitSize.width + self.margin) * i, 0, unitSize.width, unitSize.height)];
        unitView.tag = i + 1;
        
        [self addSubview:unitView];
        
        [tempArr addObject:unitView];
    }
    
    self.unitViewArr = tempArr;
    
    [self layoutIfNeeded];
}
@end
