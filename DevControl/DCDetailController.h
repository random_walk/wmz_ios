//
//  DCDetailController.h
//  DevControl
//
//  Created by rumble on 14/7/25.
//  Copyright (c) 2014年 WMZ. All rights reserved.
//

#import "DCViewController.h"

@interface DCDetailController : DCViewController

- (id)initWithDevId:(NSString *)devId;
@end
