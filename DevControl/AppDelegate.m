//
//  AppDelegate.m
//  DevControl
//
//  Created by rumble on 14/7/25.
//  Copyright (c) 2014年 WMZ. All rights reserved.
//

#import "AppDelegate.h"
#import "GCDAsyncSocket.h"
#import "DDLog.h"
#import "DDTTYLogger.h"
#import "DCMainController.h"
#import "DCNavigationController.h"

#define ENABLE_BACKGROUNDING  1

@interface AppDelegate ()
            

@end

@implementation AppDelegate
            

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    self.window.backgroundColor = [UIColor whiteColor];
    
    DCMainController *mc = [DCMainController new];
    DCNavigationController *nc = [[DCNavigationController alloc] initWithRootViewController:mc];
    self.window.rootViewController = nc;
    
    [self.window makeKeyAndVisible];
    
    dispatch_queue_t socketQueue = dispatch_queue_create("socket in background", NULL);
    asyncSocket = [[GCDAsyncSocket alloc] initWithDelegate:self delegateQueue:socketQueue];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark - public
- (GCDAsyncSocket *)getAsyncSocket
{
    return asyncSocket;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Socket Delegate
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)socket:(GCDAsyncSocket *)sock didConnectToHost:(NSString *)host port:(UInt16)port
{
    NSLog(@"socket:%p didConnectToHost:%@ port:%hu", sock, host, port);
    [sock readDataWithTimeout:-1 tag:0];

    [[NSNotificationCenter defaultCenter] postNotificationName:DCNotificationNameSocketDidConnect object:nil];
#if ENABLE_BACKGROUNDING && !TARGET_IPHONE_SIMULATOR
    {
        // Backgrounding doesn't seem to be supported on the simulator yet
        
        [sock performBlock:^{
            if ([sock enableBackgroundingOnSocket])
                NSLog(@"Enabled backgrounding on socket");
            else
                NSLog(@"Enabling backgrounding failed!");
        }];
    }
#endif
}

- (void)socketDidSecure:(GCDAsyncSocket *)sock
{
    NSLog(@"socketDidSecure:%p", sock);
    
}

- (void)socket:(GCDAsyncSocket *)sock didWriteDataWithTag:(long)tag
{
    NSLog(@"socket:%p didWriteDataWithTag:%ld", sock, tag);
    [sock readDataWithTimeout:-1 tag:0];
}

- (void)socket:(GCDAsyncSocket *)sock didReadData:(NSData *)data withTag:(long)tag
{
    NSLog(@"socket:%p didReadData:withTag:%ld", sock, tag);

    NSString *response = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    
    NSLog(@"didReadData:%@ length:%i", response, response.length);
    
    if ([response hasPrefix:@"FK"] && [response hasSuffix:@"CA\n"]) {
        
        if (response.length == 28 + 1) {
            [[NSNotificationCenter defaultCenter] postNotificationName:DCNotificationNameSocketGetDetail object:response userInfo:nil];
        }else if (response.length == 8 + 1) {
            [[NSNotificationCenter defaultCenter] postNotificationName:DCNotificationNameSocketGetCommond object:response userInfo:nil];
        }
        
    }else if ([response hasPrefix:@"List:"]) {
        [[NSNotificationCenter defaultCenter] postNotificationName:DCNotificationNameSocketGetDevList object:response userInfo:nil];
    }
    
    [sock readDataWithTimeout:-1 tag:0];
}

- (void)socketDidDisconnect:(GCDAsyncSocket *)sock withError:(NSError *)err
{
    NSLog(@"socketDidDisconnect:%p withError: %@", sock, err);
    [[NSNotificationCenter defaultCenter] postNotificationName:DCNotificationNameSocketDidDisconnect object:nil];
}

@end
