//
//  DCNavigationController.m
//  DevControl
//
//  Created by rumble on 14/7/31.
//  Copyright (c) 2014年 WMZ. All rights reserved.
//

#import "DCNavigationController.h"
#import "DCUtility.h"

@interface DCNavigationController ()

@end

@implementation DCNavigationController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSUInteger) supportedInterfaceOrientations {
    
    if (DCIsIpad()) {
        return UIInterfaceOrientationMaskAll;
    }else {
        return UIInterfaceOrientationMaskPortrait;
    }
    //
}

- (UIInterfaceOrientation) preferredInterfaceOrientationForPresentation {
    
    if (DCIsIpad()) {
        return UIInterfaceOrientationPortrait;
    }else {
        return UIInterfaceOrientationPortrait;
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
