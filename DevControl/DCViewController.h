//
//  DCViewController.h
//  DevControl
//
//  Created by rumble on 14/7/25.
//  Copyright (c) 2014年 WMZ. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DCViewController : UIViewController

@property (nonatomic) CGRect viewFrame;

- (void)myLayoutSubViews:(UIInterfaceOrientation)orientation;
- (void)socketDidDisconnect;
@end
