//
//  DCUtility.h
//  DevControl
//
//  Created by rumble on 14/7/25.
//  Copyright (c) 2014年 WMZ. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GCDAsyncSocket.h"

UIButton *DCGetCommonButton();
UILabel *DCGetCommonLabel();
void DCAlertWithTitleAndMessage(NSString *title, NSString *message);
GCDAsyncSocket *DCGetSocket();
BOOL DCConnect2Server();
void DCDisconnect();
void DCSendCommand2Server(NSString *message);

BOOL DCIsIpad();
@interface DCUtility : NSObject

@end
