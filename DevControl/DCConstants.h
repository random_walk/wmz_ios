//
//  DCConstants.h
//  DevControl
//
//  Created by rumble on 14/7/25.
//  Copyright (c) 2014年 WMZ. All rights reserved.
//

#import <Foundation/Foundation.h>

#if 1
#define HOST @"112.124.104.218"  // @"127.0.0.1"//
#else
#define HOST @"127.0.0.1"
#endif
#define PORT 8000

extern NSString * const DCNotificationNameSocketDidConnect;
extern NSString * const DCNotificationNameSocketDidDisconnect;
extern NSString * const DCNotificationNameSocketGetDevList;
extern NSString * const DCNotificationNameSocketGetDetail;
extern NSString * const DCNotificationNameSocketGetCommond;