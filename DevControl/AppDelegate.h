//
//  AppDelegate.h
//  DevControl
//
//  Created by rumble on 14/7/25.
//  Copyright (c) 2014年 WMZ. All rights reserved.
//

#import <UIKit/UIKit.h>
@class GCDAsyncSocket;

@interface AppDelegate : UIResponder <UIApplicationDelegate>
{
    GCDAsyncSocket *asyncSocket;
}

@property (strong, nonatomic) UIWindow *window;

- (GCDAsyncSocket *)getAsyncSocket;
@end

