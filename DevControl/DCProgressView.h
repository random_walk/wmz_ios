//
//  DCProgressView.h
//  DevControl
//
//  Created by rumble on 14/7/28.
//  Copyright (c) 2014年 WMZ. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DCProgressView : UIView

@property (nonatomic) NSUInteger progressValue;
- (id)initWithFrame:(CGRect)frame unitsNum:(NSInteger)unitsNum;
- (void)setMargin:(CGFloat)margin;
- (void)setSelectedColor:(UIColor *)selectedColor;
- (void)setUnSelectedColor:(UIColor *)unSelectedColor;
@end
