//
//  DCViewController.m
//  DevControl
//
//  Created by rumble on 14/7/25.
//  Copyright (c) 2014年 WMZ. All rights reserved.
//

#import "DCViewController.h"
#import "DCUtility.h"

@interface DCViewController ()

@end

@implementation DCViewController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(socketDidDisconnect) name:DCNotificationNameSocketDidDisconnect object:nil];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
    [self myLayoutSubViews:orientation];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)socketDidDisconnect
{
    if (self.navigationController.topViewController == self) {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}

- (void) willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [self myLayoutSubViews:toInterfaceOrientation];
}

- (void)myLayoutSubViews:(UIInterfaceOrientation)orientation
{
    NSLog(@"myLayoutSubViews:%@, %@", NSStringFromCGRect(self.view.frame), NSStringFromCGRect(self.view.bounds));
    
    self.viewFrame = self.view.frame;
    if (orientation == UIInterfaceOrientationLandscapeLeft || orientation == UIInterfaceOrientationLandscapeRight) {
        if (_viewFrame.size.width < _viewFrame.size.height) {
            _viewFrame = CGRectMake(_viewFrame.origin.x, _viewFrame.origin.y, _viewFrame.size.height + _viewFrame
                                    .origin.y, _viewFrame.size.width - _viewFrame.origin.y);
        }
    }else if (orientation == UIInterfaceOrientationPortrait || orientation == UIInterfaceOrientationPortraitUpsideDown) {
        if (_viewFrame.size.width > _viewFrame.size.height) {
            _viewFrame = CGRectMake(_viewFrame.origin.x, _viewFrame.origin.y, _viewFrame.size.height + _viewFrame
                                    .origin.y, _viewFrame.size.width - _viewFrame.origin.y);
        }
    }
    
}
@end
