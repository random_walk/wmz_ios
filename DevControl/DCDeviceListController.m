//
//  DCDeviceListController.m
//  DevControl
//
//  Created by rumble on 14/7/25.
//  Copyright (c) 2014年 WMZ. All rights reserved.
//

#import "DCDeviceListController.h"
#import "DCUtility.h"
#import "DCDetailController.h"

@interface DCDeviceListController ()<UITableViewDataSource, UITableViewDelegate>

@property (nonatomic,strong) UITableView    *tableView;
@property (nonatomic, strong) NSArray       *devArray;
@end

@implementation DCDeviceListController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveList:) name:DCNotificationNameSocketGetDevList object:nil];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.view addSubview:self.tableView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UITableView *)tableView
{
    if (!_tableView) {
        
        self.tableView = [[UITableView alloc] initWithFrame:self.view.bounds];
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        _tableView.dataSource = self;
        _tableView.delegate = self;
    }
    
    return _tableView;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    DCSendCommand2Server(@"WannaList");
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - noti
- (void)receiveList:(NSNotification *)noti
{
    NSString *listString = noti.object;
    listString = [listString substringFromIndex:@"List:".length];
    NSArray *listArray = [listString componentsSeparatedByString:@","];
    NSMutableArray *usefulArray = [NSMutableArray array];
    for (NSString *devId in listArray) {
        
        if (devId.length > 0 && ![devId isEqualToString:@"\n"]) {
            [usefulArray addObject:devId];
        }
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        self.devArray = [NSArray arrayWithArray:usefulArray];
        [_tableView reloadData];

    });
}

#pragma mark - UITableViewDelegate && UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _devArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([UIDevice currentDevice].userInterfaceIdiom==UIUserInterfaceIdiomPad) {
        return 120;
    }else{
        return 60;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"SendGoodsCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (!cell) {
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        cell.backgroundColor = [UIColor whiteColor];
        cell.contentView.backgroundColor = [UIColor whiteColor];
        cell.textLabel.textColor = [UIColor blackColor];
    }
    
    cell.textLabel.text = _devArray[indexPath.row];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSString *devIdString = _devArray[indexPath.row];
    DCDetailController *dc = [[DCDetailController alloc] initWithDevId:devIdString];
    [self.navigationController pushViewController:dc animated:YES];
}

@end
