//
//  DCTestController.h
//  DevControl
//
//  Created by rumble on 14/7/29.
//  Copyright (c) 2014年 WMZ. All rights reserved.
//

#import "DCViewController.h"

@interface DCTestController : DCViewController

- (instancetype)initWithDeviceId:(NSString *)devId;
@end
