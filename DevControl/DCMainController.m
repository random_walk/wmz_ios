//
//  DCMainController.m
//  DevControl
//
//  Created by rumble on 14/7/25.
//  Copyright (c) 2014年 WMZ. All rights reserved.
//

#import "DCMainController.h"
#import "UIImage+Additions.h"
#import "DCUtility.h"
#import "DCDeviceListController.h"
#import "UIView+Additions.h"
#import "DCDetailController.h"
#import "DCTestController.h"
#import "UIAlertView+Blocks.h"

typedef enum {
    kConnectionStatusUnconnect,
    kConnectionStatusDidConnect,
    kConnectionStatusDidDisconnect,
    kConnectionStatusFailed,
}ConnectionStatus;

@interface DCMainController ()

@property (nonatomic, strong) UIButton  *connectServerBtn;
@property (nonatomic, strong) UIButton  *devListBtn;
@property (nonatomic, strong) UILabel   *connectStatusLab;
@property (nonatomic, strong) UIButton  *testButton;
@property (nonatomic) ConnectionStatus  status;
@end

@implementation DCMainController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(socketDidConnect) name:DCNotificationNameSocketDidConnect object:nil];
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.view addSubview:self.connectServerBtn];
    [self.view addSubview:self.devListBtn];
    [self.view addSubview:self.connectStatusLab];
    
    [self.view addSubview:self.testButton];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self updateUIWithConnectionStatus];
}

- (UIButton *)connectServerBtn
{
    if (!_connectServerBtn) {
        
        self.connectServerBtn = DCGetCommonButton();
        [_connectServerBtn setTitle:@"连接服务" forState:UIControlStateNormal];
        [_connectServerBtn setTitle:@"断开服务" forState:UIControlStateSelected];
        [_connectServerBtn addTarget:self action:@selector(connect:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    return _connectServerBtn;
}

- (UIButton *)devListBtn
{
    if (!_devListBtn) {
        
        self.devListBtn = DCGetCommonButton();
        [_devListBtn setTitle:@"设备列表" forState:UIControlStateNormal];
        [_devListBtn addTarget:self action:@selector(goList:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    return _devListBtn;
}

- (UIButton *)testButton
{
    if (!_testButton) {
        
        self.testButton = DCGetCommonButton();
        [_testButton setTitle:@"模拟设备" forState:UIControlStateNormal];
        [_testButton addTarget:self action:@selector(test:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    return _testButton;
}

- (UILabel *)connectStatusLab
{
    if (!_connectStatusLab) {
        
        self.connectStatusLab = DCGetCommonLabel();
        _connectStatusLab.size = CGSizeMake(100, 30);
    }
    
    return _connectStatusLab;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)test:(UIButton *)button
{
    GCDAsyncSocket *socket = DCGetSocket();
    if (!socket.isConnected) {
        
        DCAlertWithTitleAndMessage(@"错误", @"请先连接服务");
    }else {
        
        RIButtonItem *okItem = [RIButtonItem itemWithLabel:@"确定"];
        
        RIButtonItem *cancelItem = [RIButtonItem itemWithLabel:@"取消"];
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"输入设备编号" message:@"" cancelButtonItem:cancelItem otherButtonItems:okItem, nil];
        alertView.alertViewStyle = UIAlertViewStylePlainTextInput;
        
        [alertView show];
        
        
        okItem.action = ^{
            
            UITextField *devField = [alertView textFieldAtIndex:0];
            
            NSCharacterSet* notDigits = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
            if (devField.text.length == 2 && [devField.text rangeOfCharacterFromSet:notDigits].location == NSNotFound)
            {
                DCTestController *tc = [[DCTestController alloc] initWithDeviceId:devField.text];
                [self.navigationController pushViewController:tc animated:YES];
            }else {
                
                DCAlertWithTitleAndMessage(@"错误", @"两位数字");
            }
            
        };
    }
}

#pragma mark - noti
- (void)socketDidConnect
{
    DCSendCommand2Server(@"ConID:00");
    [self updateUIWithConnectionStatus];
}

- (void)socketDidDisconnect
{
    self.status = kConnectionStatusDidDisconnect;
    [self updateUIWithConnectionStatus];
}

#pragma mark - private
- (void)connect:(UIButton *)button
{
//    DCDetailController *dc = [[DCDetailController alloc] initWithDevId:@"01"];
//    [self.navigationController pushViewController:dc animated:YES];
//    return;
    
    button.enabled = NO;
    
    if (!button.selected) {
        _connectStatusLab.text = @"正在连接...";
        if (!DCConnect2Server()) {
            self.status = kConnectionStatusFailed;
            [self updateUIWithConnectionStatus];
        }
    }else {
        _connectStatusLab.text = @"正在断开...";
        DCDisconnect();
        
    }
}

- (void)goList:(UIButton *)button
{
    GCDAsyncSocket *socket = DCGetSocket();
    if (!socket.isConnected) {
        
        DCAlertWithTitleAndMessage(@"错误", @"请先连接服务");
    }else {
        
        DCDeviceListController *dc = [[DCDeviceListController alloc] init];
        [self.navigationController pushViewController:dc animated:YES];
    }
}

- (void)updateUIWithConnectionStatus
{
    dispatch_async(dispatch_get_main_queue(), ^{
        _connectServerBtn.enabled = YES;
        
        GCDAsyncSocket *socket = DCGetSocket();
        if (socket.isConnected) {
            self.status = kConnectionStatusDidConnect;
            _connectStatusLab.text = @"已连接";
            _connectServerBtn.selected = YES;
        }else {
            
            if (kConnectionStatusFailed == self.status) {
                _connectStatusLab.text = @"连接失败";
            }else if (kConnectionStatusDidConnect == self.status) {
                _connectStatusLab.text = @"连接已断开";
                self.status = kConnectionStatusDidDisconnect;
            }else {
                _connectStatusLab.text = @"未连接";
            }
            
            _connectServerBtn.selected = NO;
        }
    });
}

#pragma mark - override
- (void)myLayoutSubViews:(UIInterfaceOrientation)orientation
{
    [super myLayoutSubViews:orientation];
    
    
    if([UIDevice currentDevice].userInterfaceIdiom==UIUserInterfaceIdiomPad) {
        //Device is ipad
        _connectStatusLab.left = 40;
        _connectStatusLab.top = 40;
        
        _connectServerBtn.left = 40;
        _connectServerBtn.top = 170;
        
        _devListBtn.left = _connectServerBtn.right + 70;
        _devListBtn.centerY = _connectServerBtn.centerY;
        
        _testButton.left = _devListBtn.right + 70;
        _testButton.centerY = _devListBtn.centerY;
    }else{
        //Device is iphone
        _connectStatusLab.left = 20;
        _connectStatusLab.top = 20;
        
        _connectServerBtn.left = 20;
        _connectServerBtn.top = 100;
        
        _devListBtn.left = _connectServerBtn.right + 20;
        _devListBtn.centerY = _connectServerBtn.centerY;
        
        _testButton.left = _devListBtn.right + 20;
        _testButton.centerY = _devListBtn.centerY;
    }
}
@end
