//
//  DCDetailController.m
//  DevControl
//
//  Created by rumble on 14/7/25.
//  Copyright (c) 2014年 WMZ. All rights reserved.
//

#import "DCDetailController.h"
#import "DCUtility.h"
#import "UIView+Additions.h"
#import "UIImage+Additions.h"
#import "DCProgressView.h"

@interface DCDetailController ()

@property (nonatomic, strong) NSString *currentDevId;


@property (nonatomic, strong) UILabel   *currentTitleLab;
@property (nonatomic, strong) UILabel   *currentValueLab;
@property (nonatomic, strong) DCProgressView    *currentProgressView;
@property (nonatomic, strong) UILabel   *voltTitleLab;
@property (nonatomic, strong) UILabel   *voltValueLab;
@property (nonatomic, strong) DCProgressView    *voltProgressView;
@property (nonatomic, strong) UILabel   *statusTitleLab;
@property (nonatomic, strong) UILabel   *statusLineLab;
@property (nonatomic, strong) UILabel   *statusCoolingLab;
@property (nonatomic, strong) UILabel   *statusOperLab;
@property (nonatomic, strong) UILabel   *statusTripLab;
@property (nonatomic, strong) UILabel   *failTitleLab;

@property (nonatomic, strong) UIButton  *switchOnBtn;
@property (nonatomic, strong) UIButton  *switchOffBtn;
@property (nonatomic, strong) UIImageView   *emergencyTitleView;
@property (nonatomic, strong) UIButton  *emergencyBtn;
@property (nonatomic, strong) UIButton  *resetBtn;
@property (nonatomic, strong) UILabel   *gainTitleLab;
@property (nonatomic, strong) UILabel   *gainValueLab;
@property (nonatomic, strong) DCProgressView    *gainProgressView;

@property (nonatomic, strong) UISlider  *gainSlider;
@property (nonatomic, strong) UIButton  *gainOkBtn;

@property (nonatomic, strong) NSDictionary  *failCodeDict;

@property (nonatomic) NSInteger currentValue;
@property (nonatomic) NSInteger voltValue;
@property (nonatomic) NSInteger gainValue;
@end

@implementation DCDetailController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
    }
    return self;
}

- (id)initWithDevId:(NSString *)devId
{
    self = [super initWithNibName:nil bundle:nil];
    if (self) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveDetail:) name:DCNotificationNameSocketGetDetail object:nil];
        self.currentDevId = devId;
        self.failCodeDict = @{
                              @"14" : @"OVER HEAT",
                              @"15" : @"HEAT EXCHANGER",
                              @"16" : @"FIELD SUPPLY",
                              @"17" : @"EXTERNAL STOP",
                              @"18" : @"OTHERS",
                              @"19" : @"WATER LEAK",
                              @"20" : @"EMERGENCY STOP",
                              @"21" : @"PS HIGH VOLTAGE",
                              @"22" : @"OVER DISPLACEMENT",
                              @"23" : @"PS OVER CURRENT",
                              @"24" : @"OVER CURRENT",
                              @"25" : @"PS LOW VOLTAGE",
                              @"26" : @"THERMAL RELAY",
                              @"27" : @"OVER VOLTAGE",
                              @"28" : @"MODULE",
                              };
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"返回"
                                                                   style:UIBarButtonItemStyleBordered
                                                                  target:self
                                                                  action:@selector(handleBack:)];
    self.navigationItem.leftBarButtonItem = backButton;
    self.navigationItem.title = [NSString stringWithFormat:@"设备号: %@", self.self.currentDevId];
    
    //告诉服务器，我要 currentDevId 的数据
    DCSendCommand2Server([NSString stringWithFormat:@"Request:%@", self.currentDevId]);
    
    [self.view addSubview:self.currentTitleLab];
    [self.view addSubview:self.currentProgressView];
    [self.view addSubview:self.currentValueLab];
    
    [self.view addSubview:self.voltTitleLab];
    [self.view addSubview:self.voltProgressView];
    [self.view addSubview:self.voltValueLab];
    
    [self.view addSubview:self.switchOnBtn];
    [self.view addSubview:self.switchOffBtn];
    
    [self.view addSubview:self.emergencyTitleView];
    [self.view addSubview:self.emergencyBtn];
    
    [self.view addSubview:self.resetBtn];
    [self.view addSubview:self.gainTitleLab];
    [self.view addSubview:self.gainValueLab];
    [self.view addSubview:self.gainProgressView];
    
    [self.view addSubview:self.failTitleLab];
    [self.view addSubview:self.statusTitleLab];
    [self.view addSubview:self.statusLineLab];
    [self.view addSubview:self.statusCoolingLab];
    [self.view addSubview:self.statusOperLab];
    [self.view addSubview:self.statusTripLab];
    
    _switchOnBtn.enabled = YES;
    _switchOffBtn.enabled = NO;
    _resetBtn.enabled = NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UILabel *)currentTitleLab
{
    if (!_currentTitleLab) {
        
        self.currentTitleLab = DCGetCommonLabel();
        _currentTitleLab.text = @"OUTPUT CURRENT";
        [_currentTitleLab sizeToFit];
    }
    
    return _currentTitleLab;
}

- (UILabel *)currentValueLab
{
    if (!_currentValueLab) {
        
        self.currentValueLab = DCGetCommonLabel();
        CGFloat fontsize = DCIsIpad()? 24: 16;
        _currentValueLab.font = [UIFont systemFontOfSize:fontsize];
        _currentValueLab.text = @"000A";
        [_currentValueLab sizeToFit];
        
    }
    
    return _currentValueLab;
}

- (DCProgressView *)currentProgressView
{
    if (!_currentProgressView) {
        
        CGSize progressViewSize = DCIsIpad()? CGSizeMake(400, 30): CGSizeMake(200, 20);
        self.currentProgressView = [[DCProgressView alloc] initWithFrame:CGRectMake(0, 0, progressViewSize.width, progressViewSize.height) unitsNum:10];
    }
    
    return _currentProgressView;
}

- (UILabel *)voltTitleLab
{
    if (!_voltTitleLab) {
        
        self.voltTitleLab = DCGetCommonLabel();
        _voltTitleLab.text = @"OUTPUT VOLTAGE";
        [_voltTitleLab sizeToFit];
    }
    
    return _voltTitleLab;
}


- (UILabel *)voltValueLab
{
    if (!_voltValueLab) {
        
        self.voltValueLab = DCGetCommonLabel();
        CGFloat fontsize = DCIsIpad()? 24: 16;
        _voltValueLab.font = [UIFont systemFontOfSize:fontsize];
        _voltValueLab.text = @"000V";
        [_voltValueLab sizeToFit];
    }
    
    return _voltValueLab;
}

- (DCProgressView *)voltProgressView
{
    if (!_voltProgressView) {
        
        CGSize progressViewSize = DCIsIpad()? CGSizeMake(400, 30): CGSizeMake(200, 20);
        self.voltProgressView = [[DCProgressView alloc] initWithFrame:CGRectMake(0, 0, progressViewSize.width, progressViewSize.height) unitsNum:10];
    }
    
    return _voltProgressView;
}

- (UILabel *)statusTitleLab
{
    if (!_statusTitleLab) {
        
        self.statusTitleLab = DCGetCommonLabel();
        _statusTitleLab.text = @"Status:";
        [_statusTitleLab sizeToFit];
    }
    
    return _statusTitleLab;
}

- (UILabel *)statusLineLab
{
    if (!_statusLineLab) {
        
        self.statusLineLab = DCGetCommonLabel();
        _statusLineLab.text = @"LINE";
        [_statusLineLab sizeToFit];
    }
    
    return _statusLineLab;
}

- (UILabel *)statusCoolingLab
{
    if (!_statusCoolingLab) {
        
        self.statusCoolingLab = DCGetCommonLabel();
        _statusCoolingLab.text = @"COOLING";
        [_statusCoolingLab sizeToFit];
    }
    
    return _statusCoolingLab;
}

- (UILabel *)statusOperLab
{
    if (!_statusOperLab) {
        
        self.statusOperLab = DCGetCommonLabel();
        _statusOperLab.text = @"OPER.";
        [_statusOperLab sizeToFit];
    }
    
    return _statusOperLab;
}

- (UILabel *)statusTripLab
{
    if (!_statusTripLab) {
        
        self.statusTripLab = DCGetCommonLabel();
        _statusTripLab.text = @"TRIP";
        _statusTripLab.textColor = [UIColor redColor];
        _statusTripLab.hidden = YES;
        [_statusTripLab sizeToFit];
    }
    
    return _statusTripLab;
}

- (UILabel *)failTitleLab
{
    if (!_failTitleLab) {
        
        self.failTitleLab = DCGetCommonLabel();
        _failTitleLab.text = @"Fail:";
        [_failTitleLab sizeToFit];
        _failTitleLab.textColor = [UIColor redColor];
        _failTitleLab.hidden = YES;
    }
    
    return _failTitleLab;
}

- (UIButton *)switchOnBtn
{
    if (!_switchOnBtn) {
        CGSize buttonSize = DCIsIpad()? CGSizeMake(60, 60): CGSizeMake(40, 40);
        CGFloat fontSize = 14;
        self.switchOnBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, buttonSize.width, buttonSize.height)];
        [_switchOnBtn setBackgroundImage:[UIImage imageNamedNoCache:@"btn_circle_green"] forState:UIControlStateNormal];
        [_switchOnBtn setBackgroundImage:[UIImage imageNamedNoCache:@"btn_circle_hightlight"] forState:UIControlStateHighlighted];
        [_switchOnBtn setBackgroundImage:[UIImage imageNamedNoCache:@"btn_circle_gray"] forState:UIControlStateDisabled];
        [_switchOnBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [_switchOnBtn setTitle:@"ON" forState:UIControlStateNormal];
        _switchOnBtn.titleLabel.font = [UIFont systemFontOfSize:fontSize];
        [_switchOnBtn addTarget:self action:@selector(switchOn:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    return _switchOnBtn;
}

- (UIButton *)switchOffBtn
{
    if (!_switchOffBtn) {
        CGSize buttonSize = DCIsIpad()? CGSizeMake(60, 60): CGSizeMake(40, 40);
        CGFloat fontSize = 14;
        self.switchOffBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, buttonSize.width, buttonSize.height)];
        [_switchOffBtn setBackgroundImage:[UIImage imageNamedNoCache:@"btn_circle_pressed"] forState:UIControlStateNormal];
        [_switchOffBtn setBackgroundImage:[UIImage imageNamedNoCache:@"btn_circle_hightlight"] forState:UIControlStateHighlighted];
        [_switchOffBtn setBackgroundImage:[UIImage imageNamedNoCache:@"btn_circle_gray"] forState:UIControlStateDisabled];
        [_switchOffBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        _switchOffBtn.titleLabel.font = [UIFont systemFontOfSize:fontSize];
        [_switchOffBtn setTitle:@"OFF" forState:UIControlStateNormal];
        [_switchOffBtn addTarget:self action:@selector(switchOff:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    return _switchOffBtn;
}

- (UIButton *)emergencyBtn
{
    if (!_emergencyBtn) {
        CGSize buttonSize = DCIsIpad()? CGSizeMake(70, 70): CGSizeMake(50, 50);
        //CGFloat fontSize = 14;
        self.emergencyBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, buttonSize.width, buttonSize.height)];
        [_emergencyBtn setBackgroundImage:[UIImage imageNamedNoCache:@"btn_emergency"] forState:UIControlStateNormal];
        [_emergencyBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        //_emergencyBtn.titleLabel.font = [UIFont systemFontOfSize:fontSize];
        //[_emergencyBtn setTitle:@"OFF" forState:UIControlStateNormal];
        [_emergencyBtn addTarget:self action:@selector(emergency:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    return _emergencyBtn;
}

- (UIImageView *)emergencyTitleView
{
    if (!_emergencyTitleView) {
        
        CGSize imageSize = DCIsIpad()? CGSizeMake(140, 140): CGSizeMake(100, 100);
        self.emergencyTitleView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, imageSize.width, imageSize.height)];
        [_emergencyTitleView setImage:[UIImage imageNamedNoCache:@"img_emergency"]];
    }
    
    return _emergencyTitleView;
}

- (UIButton *)resetBtn
{
    if (!_resetBtn) {
        CGSize buttonSize = DCIsIpad()? CGSizeMake(60, 60): CGSizeMake(40, 40);
        CGFloat fontSize = 10;
        self.resetBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, buttonSize.width, buttonSize.height)];
        [_resetBtn setBackgroundImage:[UIImage imageNamedNoCache:@"btn_circle"] forState:UIControlStateNormal];
        [_resetBtn setBackgroundImage:[UIImage imageNamedNoCache:@"btn_circle_hightlight"] forState:UIControlStateHighlighted];
        [_resetBtn setBackgroundImage:[UIImage imageNamedNoCache:@"btn_circle_gray"] forState:UIControlStateDisabled];
        [_resetBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        _resetBtn.titleLabel.font = [UIFont systemFontOfSize:fontSize];
        [_resetBtn setTitle:@"RESET" forState:UIControlStateNormal];
        [_resetBtn addTarget:self action:@selector(resetGain:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    return _resetBtn;
}

- (UILabel *)gainTitleLab
{
    if (!_gainTitleLab) {
        
        self.gainTitleLab = DCGetCommonLabel();
        _gainTitleLab.text = @"GAIN";
        [_gainTitleLab sizeToFit];
    }
    
    return _gainTitleLab;
}

- (UILabel *)gainValueLab
{
    if (!_gainValueLab) {
        
        self.gainValueLab = DCGetCommonLabel();
        CGFloat fontsize = DCIsIpad()? 24: 16;
        _gainValueLab.font = [UIFont systemFontOfSize:fontsize];
        _gainValueLab.textAlignment = NSTextAlignmentRight;
        _gainValueLab.text = @"100%";
        [_gainValueLab sizeToFit];
        _gainValueLab.text = @"0%";
    }
    
    return _gainValueLab;
}

- (DCProgressView *)gainProgressView
{
    if (!_gainProgressView) {
        
        CGSize progressViewSize = DCIsIpad()? CGSizeMake(400, 30): CGSizeMake(200, 20);
        self.gainProgressView = [[DCProgressView alloc] initWithFrame:CGRectMake(0, 0, progressViewSize.width, progressViewSize.height) unitsNum:10];
        
        UISwipeGestureRecognizer *swipGestureL = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipGain:)];
        swipGestureL.direction = UISwipeGestureRecognizerDirectionLeft;
        [_gainProgressView addGestureRecognizer:swipGestureL];
        
        UISwipeGestureRecognizer *swipGestureR = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipGain:)];
        swipGestureR.direction = UISwipeGestureRecognizerDirectionRight;
        [_gainProgressView addGestureRecognizer:swipGestureR];
    }
    
    return _gainProgressView;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - noti
- (void)receiveDetail:(NSNotification *)noti
{
    NSString *detailString = noti.object;
    detailString = [detailString substringWithRange:NSMakeRange(@"FK".length, detailString.length - @"FK".length - @"CA\n".length)];
    NSString *theDevId = [detailString substringToIndex:2];
    NSLog(@"theDevId:%@, currentDevId: %@" , theDevId, self.currentDevId);

    if ([theDevId isEqualToString:self.currentDevId]) {
        
        detailString = [detailString substringFromIndex:2];

        //电流值
        NSString *currentValue = [detailString substringToIndex:4];
        detailString = [detailString substringFromIndex:4];

        //电压值
        NSString *voltValue = [detailString substringToIndex:4];
        detailString = [detailString substringFromIndex:4];

        //开关机状态
        NSString *statusValue = [detailString substringToIndex:2];
        detailString = [detailString substringFromIndex:2];
        
        //增益
        NSString *gainValue = [detailString substringToIndex:2];
        //AA代表100%
        if ([gainValue isEqualToString:@"AA"]) gainValue = @"100";
        detailString = [detailString substringFromIndex:2];
        
        self.gainValue = [gainValue integerValue];
        
        //出错状态
        NSString *failStatusValue = [detailString substringToIndex:2];
        detailString = [detailString substringFromIndex:2];
        
        //出错代码
        NSString *failCodeValue = [detailString substringToIndex:2];
        detailString = [detailString substringFromIndex:2 + 2];
        
        //Line代码
        NSString *lineValue = [detailString substringToIndex:1];
        detailString = [detailString substringFromIndex:1];
        
        //Cooling代码
        NSString *coolingValue = [detailString substringToIndex:1];
        detailString = [detailString substringFromIndex:1];
        
        //Operation代码
        NSString *operationValue = [detailString substringToIndex:1];
        detailString = [detailString substringFromIndex:1];
        
        //Trip代码
        NSString *tripValue = [detailString substringToIndex:1];
        detailString = [detailString substringFromIndex:1];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            //开关机状态
            if ([statusValue isEqualToString:@"00"]) {
                _switchOffBtn.enabled = NO;
                _switchOnBtn.enabled = YES;
                _resetBtn.enabled = NO;
            }else if ([statusValue isEqualToString:@"01"]) {
                _switchOffBtn.enabled = YES;
                _switchOnBtn.enabled = NO;
                _resetBtn.enabled = YES;
            }else {
                _switchOffBtn.enabled = NO;
                _switchOnBtn.enabled = YES;
                _resetBtn.enabled = NO;
            }
            
            _gainValueLab.text = [NSString stringWithFormat:@"%i%%", _gainValue];
            [_gainProgressView setProgressValue:_gainValue / 10];
            
            if ([failStatusValue isEqualToString:@"01"]) {
                _failTitleLab.hidden = YES;
            }else {
                _failTitleLab.hidden = NO;
                NSString *failContent = _failCodeDict[failCodeValue];
                if (failContent.length > 0) {
                    
                    _failTitleLab.text = [NSString stringWithFormat:@"Fail: %@", failContent];
                    [_failTitleLab sizeToFit];
                }else {
                    _failTitleLab.hidden = YES;
                }
            }
            
            UIColor *normalColor = [UIColor greenColor];
            UIColor *failColor = [UIColor redColor];
            if ([lineValue isEqualToString:@"0"]) {
                _statusLineLab.textColor = normalColor;
            }else {
                _statusLineLab.textColor = failColor;
            }
            
            if ([coolingValue isEqualToString:@"0"]) {
                _statusCoolingLab.textColor = normalColor;
            }else {
                _statusCoolingLab.textColor = failColor;
            }
            
            if ([operationValue isEqualToString:@"0"]) {
                _statusOperLab.textColor = normalColor;
            }else {
                _statusOperLab.textColor = failColor;
            }
            
            if ([tripValue isEqualToString:@"1"]) {
                _statusTripLab.hidden = YES;
            }else {
                _statusTripLab.hidden = NO;
            }
            
            if ([tripValue isEqualToString:@"1"] && [failStatusValue isEqualToString:@"01"]) {
                
                _currentValueLab.text = currentValue;
                _voltValueLab.text = voltValue;
                
                NSInteger theCurrentValue = [[currentValue substringToIndex:3] integerValue];
                NSInteger theVoltValue = [[voltValue substringToIndex:3] integerValue];
                [_currentProgressView setProgressValue: (theCurrentValue + 50) / 100];
                [_voltProgressView setProgressValue: (theVoltValue + 50) / 100];
                
            }else {
                
                _currentValueLab.text = @"000A";
                _voltValueLab.text = @"000V";
                
                [_currentProgressView setProgressValue:0];
                [_voltProgressView setProgressValue:0];
            }
        });
    }
}

#pragma mark - private
- (void)switchOn:(UIButton *)btn
{
    NSString *message = @"FK0100CA";
    DCSendCommand2Server([NSString stringWithFormat:@"Commond:%@:%@", self.currentDevId, message]);
}

- (void)switchOff:(UIButton *)btn
{
    NSString *message = @"FK0000CA";
    DCSendCommand2Server([NSString stringWithFormat:@"Commond:%@:%@", self.currentDevId, message]);
}

- (void)emergency:(UIButton *)btn
{
    NSString *message = @"FK0300CA";
    DCSendCommand2Server([NSString stringWithFormat:@"Commond:%@:%@", self.currentDevId, message]);
}

- (void)resetGain:(UIButton *)btn
{
    NSString *message = @"FK0100CA";
    DCSendCommand2Server([NSString stringWithFormat:@"Commond:%@:%@", self.currentDevId, message]);
}

- (void)swipGain:(UISwipeGestureRecognizer *)aGesture
{
    //_switchOnBtn能点击，说明设备关着呢
    if (_switchOnBtn.enabled) {
        
        DCAlertWithTitleAndMessage(@"错误操作", @"设备尚未开启");
    }else {
        
        NSInteger gainVaule = self.gainValue;
        if (aGesture.direction == UISwipeGestureRecognizerDirectionRight) {
            gainVaule = MIN(gainVaule + 10, 100);
        }else {
            gainVaule = MAX(gainVaule - 10, 0);
        }
        
        NSString *gainString = gainVaule == 100? @"AA": [NSString stringWithFormat:@"%02d", gainVaule];
        NSString *message = [NSString stringWithFormat:@"FK01%@CA", gainString];
        NSLog(@"message:%@", message);
        DCSendCommand2Server([NSString stringWithFormat:@"Commond:%@:%@", self.currentDevId, message]);
    }
    
    [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        _gainProgressView.alpha = 0;
    } completion:^(BOOL finished) {
        _gainProgressView.alpha = 1;
    }];
}

- (void)handleBack:(id)sender
{
    DCSendCommand2Server(@"Stop");
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark - override
- (void)myLayoutSubViews:(UIInterfaceOrientation)orientation
{
    [super myLayoutSubViews:orientation];
    
    
    if([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
        //Device is ipad
        CGFloat lMargin = (orientation == UIInterfaceOrientationLandscapeLeft || orientation == UIInterfaceOrientationLandscapeRight)? 100: 40;
        CGFloat vPadding = (orientation == UIInterfaceOrientationLandscapeLeft || orientation == UIInterfaceOrientationLandscapeRight)? 50: 60;
        CGFloat hPadding = (orientation == UIInterfaceOrientationLandscapeLeft || orientation == UIInterfaceOrientationLandscapeRight)? 200: 120;
        CGFloat top = (orientation == UIInterfaceOrientationLandscapeLeft || orientation == UIInterfaceOrientationLandscapeRight)? 40: 100;
        //电流
        _currentTitleLab.left = lMargin;
        _currentTitleLab.top = top;
        
        _currentProgressView.left = lMargin;
        _currentProgressView.top = _currentTitleLab.bottom + 5;
        
        _currentValueLab.left = _currentProgressView.right + hPadding;
        _currentValueLab.centerY = _currentProgressView.centerY;
        //电压
        _voltTitleLab.left = lMargin;
        _voltTitleLab.top = _currentProgressView.bottom + vPadding;
        
        _voltProgressView.left = lMargin;
        _voltProgressView.top = _voltTitleLab.bottom + 5;
        
        _voltValueLab.left = _voltProgressView.right + hPadding;
        _voltValueLab.centerY = _voltProgressView.centerY;
        //开关，紧急
        _switchOnBtn.left = lMargin;
        _switchOnBtn.top = _voltProgressView.bottom + vPadding * 2;
        
        _switchOffBtn.left = _switchOnBtn.right + 30;
        _switchOffBtn.centerY = _switchOnBtn.centerY;
        
        _emergencyTitleView.left = _switchOffBtn.right + 40;
        _emergencyTitleView.centerY = _switchOnBtn.centerY;
        
        _emergencyBtn.center = _emergencyTitleView.center;
        //增益
        _gainTitleLab.left = lMargin;
        _gainTitleLab.top = _switchOnBtn.bottom + vPadding;
        
        _gainProgressView.left = lMargin;
        _gainProgressView.top = _gainTitleLab.bottom + 5;
        
        _gainValueLab.left = _gainProgressView.right + hPadding;
        _gainValueLab.centerY = _gainProgressView.centerY;
        
        _resetBtn.left = _gainValueLab.right + 20;
        _resetBtn.centerY = _gainProgressView.centerY;
        //错误
        _failTitleLab.left = lMargin;
        _failTitleLab.top = _gainProgressView.bottom + vPadding + 20;
        
        CGFloat padding = 30;
        _statusTitleLab.left = lMargin;
        _statusTitleLab.top = _failTitleLab.bottom + vPadding - 20;
        
        UILabel *theLabel = _statusLineLab;
        _statusLineLab.left = _statusTitleLab.right + padding;
        _statusLineLab.centerY = _statusTitleLab.centerY;
        
        theLabel = _statusCoolingLab;
        theLabel.left = _statusLineLab.right + padding;
        theLabel.centerY = _statusTitleLab.centerY;
        
        theLabel = _statusOperLab;
        theLabel.left = _statusCoolingLab.right + padding;
        theLabel.centerY = _statusTitleLab.centerY;
        
        theLabel = _statusTripLab;
        theLabel.left = _statusOperLab.right + padding;
        theLabel.centerY = _statusTitleLab.centerY;

    }else{
        //Device is iphone
        
        CGFloat lMargin = 15;
        //电流
        _currentTitleLab.left = lMargin;
        _currentTitleLab.top = 20;
        
        _currentProgressView.left = lMargin;
        _currentProgressView.top = _currentTitleLab.bottom + 5;
        
        _currentValueLab.left = _currentProgressView.right + 40;
        _currentValueLab.centerY = _currentProgressView.centerY;
        //电压
        _voltTitleLab.left = lMargin;
        _voltTitleLab.top = _currentProgressView.bottom + 15;
        
        _voltProgressView.left = lMargin;
        _voltProgressView.top = _voltTitleLab.bottom + 5;
        
        _voltValueLab.left = _voltProgressView.right + 40;
        _voltValueLab.centerY = _voltProgressView.centerY;
        //开关，紧急
        _switchOnBtn.left = lMargin;
        _switchOnBtn.top = _voltProgressView.bottom + 40;
        
        _switchOffBtn.left = _switchOnBtn.right + 15;
        _switchOffBtn.centerY = _switchOnBtn.centerY;
        
        _emergencyTitleView.left = _switchOffBtn.right + 20;
        _emergencyTitleView.centerY = _switchOnBtn.centerY;
        
        _emergencyBtn.center = _emergencyTitleView.center;
        //增益
        _gainTitleLab.left = lMargin;
        _gainTitleLab.top = _switchOnBtn.bottom + 30;
        
        _gainProgressView.left = lMargin;
        _gainProgressView.top = _gainTitleLab.bottom + 5;
        
        _gainValueLab.left = _gainProgressView.right + 5;
        _gainValueLab.centerY = _gainProgressView.centerY;
        
        _resetBtn.left = _gainValueLab.right + 15;
        _resetBtn.centerY = _gainProgressView.centerY;
        //错误
        _failTitleLab.left = lMargin;
        _failTitleLab.top = _gainProgressView.bottom + 20;
        
        CGFloat padding = 10;
        _statusTitleLab.left = lMargin;
        _statusTitleLab.top = _failTitleLab.bottom + 10;
        
        UILabel *theLabel = _statusLineLab;
        _statusLineLab.left = _statusTitleLab.right + 5;
        _statusLineLab.centerY = _statusTitleLab.centerY;
        
        theLabel = _statusCoolingLab;
        theLabel.left = _statusLineLab.right + padding;
        theLabel.centerY = _statusTitleLab.centerY;
        
        theLabel = _statusOperLab;
        theLabel.left = _statusCoolingLab.right + padding;
        theLabel.centerY = _statusTitleLab.centerY;
        
        theLabel = _statusTripLab;
        theLabel.left = _statusOperLab.right + padding;
        theLabel.centerY = _statusTitleLab.centerY;
    }
}
@end
