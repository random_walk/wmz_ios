//
//  DCTestController.m
//  DevControl
//
//  Created by rumble on 14/7/29.
//  Copyright (c) 2014年 WMZ. All rights reserved.
//

#import "DCTestController.h"
#import "DCUtility.h"
#import "UIView+Additions.h"

@interface DCTestController ()<UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate>

@property (nonatomic, strong) UISegmentedControl    *switchSegmentControl;

@property (nonatomic, strong) UILabel       *currentTitleLab;
@property (nonatomic, strong) UITextField   *currentField;
@property (nonatomic, strong) UIPickerView  *currentPickerView;

@property (nonatomic, strong) UILabel       *voltTitleLab;
@property (nonatomic, strong) UITextField   *voltField;
@property (nonatomic, strong) UIPickerView  *voltPickerView;

@property (nonatomic, strong) UILabel       *gainTitleLab;
@property (nonatomic, strong) UITextField   *gainField;
@property (nonatomic, strong) UIPickerView  *gainPickerView;

@property (nonatomic, strong) UILabel       *failTitleLab;
@property (nonatomic, strong) UITextField   *failField;
@property (nonatomic, strong) UIPickerView  *failPickerView;

@property (nonatomic, strong) NSArray       *statusArray;

@property (nonatomic, strong) NSString  *devId;

@property (nonatomic, strong) NSArray   *statusTitleArr;
@property (nonatomic, strong) NSArray  *failCodeArr;
@property (nonatomic, strong) NSArray   *gainValueArr;
@property (nonatomic, strong) NSTimer   *timer;
@end

@implementation DCTestController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (instancetype)initWithDeviceId:(NSString *)devId
{
    self = [super initWithNibName:nil bundle:nil];
    if (self) {
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveComnond:) name:DCNotificationNameSocketGetCommond object:nil];
        
        self.devId = devId;
        self.statusTitleArr = @[@"LINE", @"COOLING", @"OPER.", @"TRIP"];
        self.failCodeArr = @[
                              @"00 NONE",
                              @"14 OVER HEAT",
                              @"15 HEAT EXCHANGER",
                              @"16 FIELD SUPPLY",
                              @"17 EXTERNAL STOP",
                              @"18 OTHERS",
                              @"19 WATER LEAK",
                              @"20 EMERGENCY STOP",
                              @"21 PS HIGH VOLTAGE",
                              @"22 OVER DISPLACEMENT",
                              @"23 PS OVER CURRENT",
                              @"24 OVER CURRENT",
                              @"25 PS LOW VOLTAGE",
                              @"26 THERMAL RELAY",
                              @"27 OVER VOLTAGE",
                              @"28 MODULE",
                              ];
        self.gainValueArr = @[
                              @[@"0", @"1", @"2", @"3", @"4", @"5", @"6", @"7", @"8", @"9", @"A"],
                              @[@"0", @"A"],
                              ];
        
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"返回"
                                                                   style:UIBarButtonItemStyleBordered
                                                                  target:self
                                                                  action:@selector(handleBack:)];
    self.navigationItem.leftBarButtonItem = backButton;
    self.navigationItem.title = [NSString stringWithFormat:@"模拟设备号: %@", self.devId];
    
    [self.view addSubview:self.switchSegmentControl];
    
    [self.view addSubview:self.currentTitleLab];
    [self.view addSubview:self.currentField];
    
    [self.view addSubview:self.voltTitleLab];
    [self.view addSubview:self.voltField];

    [self.view addSubview:self.gainTitleLab];
    [self.view addSubview:self.gainField];
    
    [self.view addSubview:self.failTitleLab];
    [self.view addSubview:self.failField];
    
    [self setupStatusButtons];
    
    self.timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(goTimer:) userInfo:nil repeats:YES];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UISegmentedControl *)switchSegmentControl
{
    if (!_switchSegmentControl) {
        
        self.switchSegmentControl = [[UISegmentedControl alloc] initWithItems:@[@"OFF", @"ON"]];
        _switchSegmentControl.selectedSegmentIndex = 1;
        if (DCIsIpad()) {
            _switchSegmentControl.width += 100;
            _switchSegmentControl.height += 20;
        }
        [_switchSegmentControl addTarget:self action:@selector(changeSwitch:) forControlEvents:UIControlEventValueChanged];
    }
    
    return _switchSegmentControl;
}

- (UILabel *)currentTitleLab
{
    if (!_currentTitleLab) {
        
        self.currentTitleLab = DCGetCommonLabel();
        _currentTitleLab.text = @"CURRENT";
        [_currentTitleLab sizeToFit];
    }
    
    return _currentTitleLab;
}

- (UITextField *)currentField
{
    if (!_currentField) {
        
        self.currentField = [self commonField];
        _currentField.text = @"000A";
        _currentField.inputView = self.currentPickerView;
        _currentField.inputAccessoryView = [self commonInputAccessoryView];
    }
    
    return _currentField;
}

- (UIPickerView *)currentPickerView
{
    if (!_currentPickerView) {
        
        self.currentPickerView = [[UIPickerView alloc] init];
        UIPickerView *pickerView = _currentPickerView;
        pickerView.delegate = self;
        pickerView.dataSource = self;
    }
    
    return _currentPickerView;
}

- (UILabel *)voltTitleLab
{
    if (!_voltTitleLab) {
        
        self.voltTitleLab = DCGetCommonLabel();
        _voltTitleLab.text = @"VOLTAGE";
        [_voltTitleLab sizeToFit];
    }
    
    return _voltTitleLab;
}

- (UITextField *)voltField
{
    if (!_voltField) {
        
        self.voltField = [self commonField];
        _voltField.text = @"000V";
        _voltField.inputView = self.voltPickerView;
        _voltField.inputAccessoryView = [self commonInputAccessoryView];
    }
    
    return _voltField;
}

- (UIPickerView *)voltPickerView
{
    if (!_voltPickerView) {
        
        self.voltPickerView = [[UIPickerView alloc] init];
        UIPickerView *pickerView = _voltPickerView;
        pickerView.delegate = self;
        pickerView.dataSource = self;
    }
    
    return _voltPickerView;
}

- (UILabel *)gainTitleLab
{
    if (!_gainTitleLab) {
        
        self.gainTitleLab = DCGetCommonLabel();
        _gainTitleLab.text = @"GAIN";
        [_gainTitleLab sizeToFit];
    }
    
    return _gainTitleLab;
}

- (UITextField *)gainField
{
    if (!_gainField) {
        
        self.gainField = [self commonField];
        _gainField.text = @"00";
        _gainField.inputView = self.gainPickerView;
        _gainField.inputAccessoryView = [self commonInputAccessoryView];
    }
    
    return _gainField;
}

- (UIPickerView *)gainPickerView
{
    if (!_gainPickerView) {
        
        self.gainPickerView = [[UIPickerView alloc] init];
        UIPickerView *pickerView = _gainPickerView;
        pickerView.delegate = self;
        pickerView.dataSource = self;
    }
    
    return _gainPickerView;
}

- (UILabel *)failTitleLab
{
    if (!_failTitleLab) {
        
        self.failTitleLab = DCGetCommonLabel();
        _failTitleLab.text = @"FAIL";
        [_failTitleLab sizeToFit];
    }
    
    return _failTitleLab;
}

- (UITextField *)failField
{
    if (!_failField) {
        
        self.failField = [self commonField];
        CGFloat fontsize = DCIsIpad()? 21: 16;
        _failField.font = [UIFont systemFontOfSize:fontsize];
        _failField.width += 100;
        _failField.text = _failCodeArr[0];
        _failField.inputView = self.failPickerView;
        _failField.inputAccessoryView = [self commonInputAccessoryView];
    }
    
    return _failField;
}

- (UIPickerView *)failPickerView
{
    if (!_failPickerView) {
        
        self.failPickerView = [[UIPickerView alloc] init];
        UIPickerView *pickerView = _failPickerView;
        pickerView.delegate = self;
        pickerView.dataSource = self;
    }
    
    return _failPickerView;
}

- (UITextField *)commonField
{
    CGSize fieldSize = DCIsIpad()? CGSizeMake(160, 40): CGSizeMake(100, 30);
    CGFloat fontsize = DCIsIpad()? 24: 18;
    
    UITextField *commonField = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, fieldSize.width, fieldSize.height)];
    commonField.delegate = self;
    commonField.font = [UIFont systemFontOfSize:fontsize];
    commonField.textAlignment = NSTextAlignmentCenter;
    commonField.layer.borderColor = [UIColor colorWithRed:22 / 255. green:111 / 255. blue:155 / 255. alpha:1].CGColor;
    commonField.layer.borderWidth = 1;
    commonField.backgroundColor = [UIColor clearColor];
    commonField.textColor = [UIColor blackColor];
    commonField.layer.cornerRadius = 5;
    
    return commonField;
}

- (UIView *)commonInputAccessoryView
{
    UIView *inputAccessoryView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.width, 40)];
    inputAccessoryView.backgroundColor = [UIColor lightGrayColor];
    inputAccessoryView.layer.borderColor = [UIColor grayColor].CGColor;
    inputAccessoryView.layer.borderWidth = 0.5;
    
    UIButton *cancelButton = [UIButton new];
    cancelButton.backgroundColor = [UIColor clearColor];
    [cancelButton setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    cancelButton.titleLabel.font = [UIFont systemFontOfSize:18];
    [cancelButton setTitle:@"取消" forState:UIControlStateNormal];
    [cancelButton addTarget:self action:@selector(pickerViewCanceled:) forControlEvents:UIControlEventTouchUpInside];
    [cancelButton sizeToFit];
    
    UIButton *okButton = [UIButton new];
    okButton.backgroundColor = [UIColor clearColor];
    [okButton setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    okButton.titleLabel.font = [UIFont systemFontOfSize:18];
    [okButton setTitle:@"确定" forState:UIControlStateNormal];
    [okButton addTarget:self action:@selector(pickerViewConfirmed:) forControlEvents:UIControlEventTouchUpInside];
    [okButton sizeToFit];
    
    [inputAccessoryView addSubview:cancelButton];
    [inputAccessoryView addSubview:okButton];
    
    cancelButton.left = 10;
    cancelButton.centerY = inputAccessoryView.height / 2;
    
    okButton.right = inputAccessoryView.width - 10;
    okButton.centerY = inputAccessoryView.height / 2;
    
    return inputAccessoryView;
}

#pragma mark - private
- (void)pickerViewCanceled:(id)sender
{
    UITextField *selectedField = nil;
    
    if (_currentField.isFirstResponder) {
        selectedField = _currentField;
    }else if (_voltField.isFirstResponder) {
        selectedField = _voltField;
    }else if (_gainField.isFirstResponder) {
        selectedField = _gainField;
    }else if (_failField.isFirstResponder) {
        selectedField = _failField;
    }
    
    [selectedField resignFirstResponder];
}

- (void)pickerViewConfirmed:(id)sender
{
    UITextField *selectedField = nil;
    
    if (_currentField.isFirstResponder) {
        selectedField = _currentField;
        
        NSInteger componentOne = [_currentPickerView selectedRowInComponent:0];
        NSInteger componentTwo = [_currentPickerView selectedRowInComponent:1];
        NSInteger componentThree = [_currentPickerView selectedRowInComponent:2];
        
        selectedField.text = [NSString stringWithFormat:@"%i%i%iA", componentOne, componentTwo, componentThree];
    }else if (_voltField.isFirstResponder) {
        selectedField = _voltField;
        NSInteger componentOne = [_voltPickerView selectedRowInComponent:0];
        NSInteger componentTwo = [_voltPickerView selectedRowInComponent:1];
        NSInteger componentThree = [_voltPickerView selectedRowInComponent:2];
        
        selectedField.text = [NSString stringWithFormat:@"%i%i%iV", componentOne, componentTwo, componentThree];
    }else if (_gainField.isFirstResponder) {
        selectedField = _gainField;
        NSInteger componentOne = [_gainPickerView selectedRowInComponent:0];
        NSInteger componentTwo = [_gainPickerView selectedRowInComponent:1];
        
        selectedField.text = [NSString stringWithFormat:@"%@%@", _gainValueArr[0][componentOne], _gainValueArr[1][componentTwo]];
    }else if (_failField.isFirstResponder) {
        selectedField = _failField;
        NSInteger componentOne = [_failPickerView selectedRowInComponent:0];
        selectedField.text = _failCodeArr[componentOne];
    }
    
    [selectedField resignFirstResponder];
}

- (void)changeSwitch:(UISegmentedControl *)segmentControl
{
    
}

- (void)setupStatusButtons
{
    NSMutableArray *tempArr = [NSMutableArray array];
    CGFloat fontsize = DCIsIpad()? 24: 18;
    
    for (int i = 0; i < self.statusTitleArr.count; i++) {
        
        UIButton *button = [[UIButton alloc] init];
        button.titleLabel.font = [UIFont systemFontOfSize:fontsize];
        [button setTitle:_statusTitleArr[i] forState:UIControlStateNormal];
        [button sizeToFit];
        button.height += 8;
        button.backgroundColor = [UIColor clearColor];
        [button setTitleColor:[UIColor greenColor] forState:UIControlStateNormal];
        [button setTitleColor:[UIColor redColor] forState:UIControlStateSelected];
        button.tag = i + 1;
        if (i == self.statusTitleArr.count - 1) {
            [button setTitleColor:[UIColor greenColor] forState:UIControlStateSelected];
            [button setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
             button.selected = YES;
        }
        
        [button addTarget:self action:@selector(statusButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        [tempArr addObject:button];
        [self.view addSubview:button];
    }
    
    self.statusArray = [NSArray arrayWithArray:tempArr];
}

- (void)statusButtonClicked:(UIButton *)button
{
    button.selected = !button.selected;
}

- (void)goTimer:(id)sender
{
    NSLog(@"goTimer");
    NSMutableString *message = [NSMutableString stringWithString:@"FK"];

    //设备编号
    [message appendString: _devId];
    
    //电流
    [message appendString:_currentField.text];
    
    //电压
    [message appendString:_voltField.text];
    
    //开关
    [message appendFormat:@"%02d", _switchSegmentControl.selectedSegmentIndex];
    
    //增益
    [message appendString:_gainField.text];
    
    //错误状态
    NSInteger failCodeIndex = [_failCodeArr indexOfObject:_failField.text];
    if (failCodeIndex == 0) {
        [message appendString:@"01"];
        [message appendString:@"00"];
    }else {
        [message appendString:@"00"];
        [message appendString:[_failField.text substringToIndex:2]];
    }
    
    //不知道的两位
    [message appendString:@"00"];
    
    //四个状态
    for (UIButton *button in self.statusArray) {
        [message appendFormat:@"%i", button.selected];
    }
    
    [message appendString:@"CA"];
    
    DCSendCommand2Server(message);
}

- (void)handleBack:(id)sender
{
    [_timer invalidate];
    _timer = nil;
    
    DCDisconnect();
    
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark - noti
- (void)receiveComnond:(NSNotification *)noti
{
    NSString *detailString = noti.object;
    detailString = [detailString substringWithRange:NSMakeRange(@"FK".length, detailString.length - @"FK".length - @"CA\n".length)];
    NSString *swithStatus = [detailString substringToIndex:2];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        if ([swithStatus isEqualToString:@"01"]) {
            _switchSegmentControl.selectedSegmentIndex = 1;
        }else if ([swithStatus isEqualToString:@"00"] || [swithStatus isEqualToString:@"03"]) {
            _switchSegmentControl.selectedSegmentIndex = 0;
        }
        
        NSString *gainValue = [detailString substringFromIndex:2];
        _gainField.text = gainValue;
    });
}
#pragma mark - UITextFieldDelegate
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField == _currentField) {
        
        for (int i = 0; i < 3; i++) {
            NSString *component = [_currentField.text substringWithRange:NSMakeRange(i, 1)];
            [_currentPickerView selectRow:[component integerValue] inComponent:i animated:NO];
        }
    }else if (textField == _voltField) {
        for (int i = 0; i < 3; i++) {
            NSString *component = [_voltField.text substringWithRange:NSMakeRange(i, 1)];
            [_voltPickerView selectRow:[component integerValue] inComponent:i animated:NO];
        }
    }else if (textField == _gainField) {
        for (int i = 0; i < 2; i++) {
            NSString *component = [_gainField.text substringWithRange:NSMakeRange(i, 1)];
            NSArray *componentStringArr = _gainValueArr[i];
            [_gainPickerView selectRow:[componentStringArr indexOfObject:component] inComponent:i animated:NO];
        }
    }else if (textField == _failField) {
        NSInteger row = [_failCodeArr indexOfObject:_failField.text];
        [_gainPickerView selectRow:row inComponent:0 animated:NO];
    }else {
        
    }
}

#pragma mark - UIPickerViewDelegate
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    if (_currentPickerView == pickerView || _voltPickerView == pickerView) {
        return 3;
    }else if (_gainPickerView == pickerView) {
        return 2;
    }else if (_failPickerView == pickerView) {
        return 1;
    }else {
        return 0;
    }
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (pickerView == _failPickerView) {
        return _failCodeArr.count;
    }else if (_gainPickerView == pickerView && component == 0) {
        return 11;
    }else if (_gainPickerView == pickerView && component == 1) {
        return 2;
    }else {
        return 10;
    }
    
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    return 30;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
    if (_currentPickerView == pickerView || _voltPickerView == pickerView) {
        return self.view.width / 3;
    }else if (_gainPickerView == pickerView) {
        return self.view.width / 2;
    }else if (_failPickerView == pickerView) {
        return self.view.width;
    }else {
        return 0;
    }
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if (pickerView == _failPickerView) {
        return _failCodeArr[row];
    }if (pickerView == _gainPickerView) {
        return _gainValueArr[component][row];
    }else {
        return [NSString stringWithFormat:@"%i", row];
    }
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    
}
#pragma mark - override
- (void)myLayoutSubViews:(UIInterfaceOrientation)orientation
{
    [super myLayoutSubViews:orientation];
    
    
    if([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
        //Device is ipad
        CGFloat lMargin = (orientation == UIInterfaceOrientationLandscapeLeft || orientation == UIInterfaceOrientationLandscapeRight)? 245: 100;
        CGFloat top = (orientation == UIInterfaceOrientationLandscapeLeft || orientation == UIInterfaceOrientationLandscapeRight)? 50: 100;
        
        _switchSegmentControl.centerX = self.viewFrame.size.width / 2;
        _switchSegmentControl.top = top;
        
        CGFloat vPadding = 70;
        //电流
        _currentTitleLab.left = lMargin;
        _currentTitleLab.top = _switchSegmentControl.bottom + vPadding + 30;
        
        _currentField.left = _currentTitleLab.right + 140;
        _currentField.centerY = _currentTitleLab.centerY;
        
        //电压
        _voltTitleLab.right = _currentTitleLab.right;
        _voltTitleLab.top = _currentTitleLab.bottom + vPadding;
        
        _voltField.left = _currentField.left;
        _voltField.centerY = _voltTitleLab.centerY;
        
        //增益
        _gainTitleLab.right = _voltTitleLab.right;
        _gainTitleLab.top = _voltTitleLab.bottom + vPadding;
        _gainField.left = _voltField.left;
        _gainField.centerY = _gainTitleLab.centerY;
        
        //Fail
        _failTitleLab.right = _gainTitleLab.right;
        _failTitleLab.top = _gainTitleLab.bottom + vPadding;
        _failField.left = _gainField.left;
        _failField.centerY = _failTitleLab.centerY;
        
        //status array
        CGFloat nextX = lMargin;
        CGFloat nextY = _failTitleLab.bottom + vPadding + 40;
        CGFloat padding = 70;
        for (int i = 0; i < self.statusArray.count; i++) {
            
            UIButton *button = _statusArray[i];
            button.left = nextX;
            button.top = nextY;
            nextX += button.width + padding;
        }

    }else{
        //Device is iphone
        
        CGFloat lMargin = 15;
        _switchSegmentControl.centerX = self.viewFrame.size.width / 2;
        _switchSegmentControl.top = 20;
        
        CGFloat vPadding = 30;
        //电流
        _currentTitleLab.left = lMargin;
        _currentTitleLab.top = _switchSegmentControl.bottom + 40;
        
        _currentField.left = _currentTitleLab.right + 20;
        _currentField.centerY = _currentTitleLab.centerY;
        
        //电压
        _voltTitleLab.right = _currentTitleLab.right;
        _voltTitleLab.top = _currentTitleLab.bottom + vPadding;
        
        _voltField.left = _currentField.left;
        _voltField.centerY = _voltTitleLab.centerY;
        
        //增益
        _gainTitleLab.right = _voltTitleLab.right;
        _gainTitleLab.top = _voltTitleLab.bottom + vPadding;
        _gainField.left = _voltField.left;
        _gainField.centerY = _gainTitleLab.centerY;
        
        //Fail
        _failTitleLab.right = _gainTitleLab.right;
        _failTitleLab.top = _gainTitleLab.bottom + vPadding;
        _failField.left = _gainField.left;
        _failField.centerY = _failTitleLab.centerY;
        
        //status array
        CGFloat nextX = lMargin;
        CGFloat nextY = _failTitleLab.bottom + vPadding + 20;
        CGFloat padding = 15;
        for (int i = 0; i < self.statusArray.count; i++) {
            
            UIButton *button = _statusArray[i];
            button.left = nextX;
            button.top = nextY;
            nextX += button.width + padding;
        }
    }
}
@end
