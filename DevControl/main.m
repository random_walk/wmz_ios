//
//  main.m
//  DevControl
//
//  Created by rumble on 14/7/25.
//  Copyright (c) 2014年 WMZ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
