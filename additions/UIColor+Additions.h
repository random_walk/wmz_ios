//
//  UIColor+Additions.h
//  laundrytool
//
//  Created by rumble on 14-5-30.
//  Copyright (c) 2014年 mol. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (Additions)
+ (UIColor *) colorWithHexString: (NSString *) hexString;
@end
