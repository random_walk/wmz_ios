//
//  UITabBarItem+Additions.h
//  qianlieye
//
//  系统版本兼容
//
//  Created by rumble on 14-2-24.
//  Copyright (c) 2014年 paixiu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITabBarItem (Additions)

- (id)initWithTitle:(NSString *)title image:(UIImage *)image selectedImage:(UIImage *)selectedImage useOriginalImage:(BOOL)useOriginalImage;

@end
