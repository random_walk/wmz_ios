//
//  NSDate+Additions.h
//  qianlieye
//
//  Created by rumble on 14-3-10.
//  Copyright (c) 2014年 paixiu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (Additions)

- (NSString *)howLongAgo;

@end
