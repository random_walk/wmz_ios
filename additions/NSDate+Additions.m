//
//  NSDate+Additions.m
//  qianlieye
//
//  Created by rumble on 14-3-10.
//  Copyright (c) 2014年 paixiu. All rights reserved.
//

#import "NSDate+Additions.h"

@implementation NSDate (Additions)

- (NSString *)howLongAgo
{
    NSTimeInterval diff = [[NSDate date] timeIntervalSince1970] - [self timeIntervalSince1970];
	if (diff < 0) {
        return NSLocalizedString(@"just now", nil);
    }
    //秒
    NSUInteger seconds = (NSUInteger)diff;
	if (seconds < 10) {
		return NSLocalizedString(@"just now", nil);
	}else if (seconds < 60) {
        return [NSString stringWithFormat:NSLocalizedString(@"%u seconds ago", nil), seconds];
	}
	
    //分钟
	NSUInteger minutes = (NSUInteger)(diff / 60);
	if (minutes < 60) {
		if (minutes <= 1) {
			return [NSString stringWithFormat:NSLocalizedString(@"%u minute ago", nil), 1];
		}else {
			return [NSString stringWithFormat:NSLocalizedString(@"%u minutes ago", nil), minutes];
		}
	}
    
    //小时
	NSUInteger hours = (NSUInteger)(diff / (60 * 60));
	if (hours < 24) {
		if (hours <= 1) {
			return [NSString stringWithFormat:NSLocalizedString(@"%u hour ago", nil), 1];
		}else {
			return [NSString stringWithFormat:NSLocalizedString(@"%u hours ago", nil), hours];
		}
	}
	
    //天
    NSUInteger days = (NSUInteger)(diff / (60 * 60 * 24));
	if (days < 7) {
        if(days <= 1) {
			return [NSString stringWithFormat:NSLocalizedString(@"%u day ago", nil), 1];
		}else {
			return [NSString stringWithFormat:NSLocalizedString(@"%u days ago", nil), days];
		}
	}
	
    //周
    NSUInteger weeks = (NSUInteger)(diff / (60 * 60 * 24 * 7));
	if (weeks < 4) {
        if(weeks <= 1) {
			return [NSString stringWithFormat:NSLocalizedString(@"%u week ago", nil), 1];
		}else {
			return [NSString stringWithFormat:NSLocalizedString(@"%u weeks ago", nil), weeks];
		}
	}
    
    //月
    NSUInteger months = (NSUInteger)(diff / (60 * 60 * 24 * 30.5));
	if (months < 12) {
        if(months <= 1) {
			return [NSString stringWithFormat:NSLocalizedString(@"%u month ago", nil), 1];
		}else {
			return [NSString stringWithFormat:NSLocalizedString(@"%u months ago", nil), months];
		}
	}
    
    //年
    NSUInteger years = (NSUInteger)(diff / (60 * 60 * 24 * 365.25));
    if(years <= 1) {
        return [NSString stringWithFormat:NSLocalizedString(@"%u year ago", nil), 1];
    }else {
        return [NSString stringWithFormat:NSLocalizedString(@"%u years ago", nil), years];
    }
}
@end
