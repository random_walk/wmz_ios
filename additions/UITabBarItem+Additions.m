//
//  UITabBarItem+Additions.m
//  qianlieye
//
//  Created by rumble on 14-2-24.
//  Copyright (c) 2014年 paixiu. All rights reserved.
//

#import "UITabBarItem+Additions.h"
#import "DDUtility.h"

@implementation UITabBarItem (Additions)

- (id)initWithTitle:(NSString *)title image:(UIImage *)image selectedImage:(UIImage *)selectedImage useOriginalImage:(BOOL)useOriginalImage {
    UITabBarItem *item;
    if (DUSystemVersionGreatOrEqualTo(@"7.0")) {
        if (useOriginalImage) {
            image = [image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
            selectedImage = [selectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        }
        item = [[UITabBarItem alloc] initWithTitle:title image:image selectedImage:selectedImage];
    }else {
        item = [[UITabBarItem alloc] init];
        item.title = title;
        [item setFinishedSelectedImage:selectedImage withFinishedUnselectedImage:image];
    }
    return item;
}

@end
